package eu4.comsim.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.Army;
import eu4.comsim.core.BattleStats;
import eu4.comsim.core.SimulationController;

/**
 * Main class that houses the Shell and the main() method.<br>
 * TODO Might be sensible to refactor into 2 dedicated controller and gui
 * classes later
 */
public class CombatSimulator {

	private static final String VERSION = "1.1";
	private static final String FONT = "Segoe UI";
	private static final int MIN_HANDICAP = -3;
	private static final int MAX_HANDICAP = 3;
	private static final String TOOLTIP_HANDICAP = "Select a handicap for the attacking army (+3 to -3). Value will be added (up to a maximum of 9, down to a minimum of 0) to each single random die roll.";
	private static final String APPLICATION_ICON = "/general/EU4_icon.png";
	private static final int OPTIMAL_HEIGHT = 930;
	private static final int OPTIMAL_WIDTH = 930;
	private static final String HELP_MESSAGE = "Version 1.0 (beta). You can help improve this program by submitting comments, feature requests and bugs at https://bitbucket.org/Hottemax/eu4-combatsimulator/issues";
	/** Everyone needs a shell */
	protected Shell shlEuCombatSimulator;
	Spinner spinner_numberOfRuns;

	/**
	 * Launch the application.
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		CombatSimulator window = new CombatSimulator();
		try {
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
			MessageBox messageBox = new MessageBox(new Shell(Display.getDefault()), SWT.ICON_ERROR);
			messageBox.setMessage("Unexpected error:" + e.toString()
					+ "An error.log has been generated. You can help improve this program by submitting bugs at https://bitbucket.org/Hottemax/eu4-combatsimulator/issues");

			messageBox.open();
			File errorLog = new File("error.log");
			try (PrintStream ps = new PrintStream(errorLog);) {
				e.printStackTrace(ps);
			} catch (FileNotFoundException e1) {
				// IGNORE
			}
		}

	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlEuCombatSimulator.open();
		shlEuCombatSimulator.layout();
		while (!shlEuCombatSimulator.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlEuCombatSimulator = new Shell();
		shlEuCombatSimulator.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shlEuCombatSimulator.setImage(SWTResourceManager.getImage(CombatSimulator.class, APPLICATION_ICON));
		shlEuCombatSimulator.setSize(OPTIMAL_WIDTH, OPTIMAL_HEIGHT);
		shlEuCombatSimulator.setMinimumSize(OPTIMAL_WIDTH, OPTIMAL_HEIGHT);
		shlEuCombatSimulator.setText("EU4 Combat Simulator " + VERSION);
		GridLayout gl_shlEuCombatSimulator = new GridLayout(3, false);
		shlEuCombatSimulator.setLayout(gl_shlEuCombatSimulator);

		TerrainGroup trngrpTerrain = new TerrainGroup(shlEuCombatSimulator, SWT.NONE);
		trngrpTerrain.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 3, 1));
		ArmyGroup attackerGroup = new ArmyGroup(shlEuCombatSimulator, SWT.NONE, "Attacker");
		attackerGroup.getTechnologyGroup().setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		attackerGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Composite composite_simulation = new Composite(shlEuCombatSimulator, SWT.NONE);
		composite_simulation.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		composite_simulation.setLayout(new GridLayout(2, true));

		Group group_luck = new Group(composite_simulation, SWT.NONE);
		group_luck.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		GridLayout gl_group_luck = new GridLayout(2, true);
		gl_group_luck.verticalSpacing = 1;
		group_luck.setLayout(gl_group_luck);

		Label lblRollHandicap = new Label(group_luck, SWT.NONE);
		lblRollHandicap.setAlignment(SWT.CENTER);
		lblRollHandicap.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, true, false, 2, 1));
		lblRollHandicap.setText("Roll Handicap");

		Spinner combo_attackerRoll = new Spinner(group_luck, SWT.BORDER);
		combo_attackerRoll.setToolTipText(TOOLTIP_HANDICAP);
		combo_attackerRoll.setMaximum(MAX_HANDICAP);
		combo_attackerRoll.setMinimum(MIN_HANDICAP);

		Spinner combo_defenderRoll = new Spinner(group_luck, SWT.BORDER);
		combo_defenderRoll.setToolTipText(TOOLTIP_HANDICAP);
		combo_defenderRoll.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		combo_defenderRoll.setMaximum(MAX_HANDICAP);
		combo_defenderRoll.setMinimum(MIN_HANDICAP);

		Button btnSimulate = new Button(composite_simulation, SWT.NONE);
		btnSimulate.setToolTipText(
				"Run the simulation. The same battle will be initialized the number of times specified by the \"Runs\" setting. Results will display average values over this sample size.");
		btnSimulate.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		btnSimulate.setFont(SWTResourceManager.getFont(FONT, 10, SWT.BOLD));
		btnSimulate.setImage(SWTResourceManager.getImage(CombatSimulator.class, "/general/Prestige_from_land.png"));
		btnSimulate.setBackgroundImage(null);
		btnSimulate.setText("Simulate");

		ProgressBar progressBar = new ProgressBar(composite_simulation, SWT.SMOOTH);
		progressBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		progressBar.setMaximum(1000);

		Label lblRuns = new Label(composite_simulation, SWT.NONE);
		lblRuns.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		lblRuns.setText("Samples:");
		lblRuns.setToolTipText(
				"How many times the battle will be simulated, minimizing the effect of the dice roll randomness. More iterations produce a more accurate result, but take longer");

		spinner_numberOfRuns = new Spinner(composite_simulation, SWT.BORDER);
		spinner_numberOfRuns.setToolTipText("Sample size (1-1000)");
		spinner_numberOfRuns.setSelection(100);
		spinner_numberOfRuns.setMinimum(1);
		spinner_numberOfRuns.setMaximum(1000);

		ArmyGroup defenderGroup = new ArmyGroup(shlEuCombatSimulator, SWT.NONE, "Defender");
		defenderGroup.getTechnologyGroup().setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		defenderGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Label label_resultseparator = new Label(shlEuCombatSimulator,
				SWT.BORDER | SWT.SEPARATOR | SWT.HORIZONTAL | SWT.SHADOW_NONE);
		label_resultseparator.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_NORMAL_SHADOW));
		label_resultseparator.setFont(SWTResourceManager.getFont(FONT, 20, SWT.BOLD));
		label_resultseparator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));

		ResultsComposite results_attacker = new ResultsComposite(shlEuCombatSimulator, SWT.NONE);
		results_attacker.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));

		Composite composite_overallResults = new Composite(shlEuCombatSimulator, SWT.NONE);
		composite_overallResults.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite_overallResults.setLayout(new GridLayout(2, true));

		Label label = new Label(composite_overallResults, SWT.NONE);
		label.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		label.setText("Avg. rolls");
		label.setAlignment(SWT.CENTER);

		Label label_averageRollA = new Label(composite_overallResults, SWT.BORDER | SWT.SHADOW_NONE);
		label_averageRollA.setAlignment(SWT.CENTER);
		GridData gd_label_averageRollA = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_label_averageRollA.minimumWidth = 20;
		label_averageRollA.setLayoutData(gd_label_averageRollA);
		label_averageRollA.setToolTipText("Average attacker roll (includes handicap!).");
		label_averageRollA.setText("buuuuuh");
		label_averageRollA.setVisible(false);

		Label label_averageRollB = new Label(composite_overallResults, SWT.BORDER | SWT.SHADOW_NONE | SWT.RIGHT);
		label_averageRollB.setAlignment(SWT.CENTER);
		GridData gd_label_averageRollB = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_label_averageRollB.minimumWidth = 20;
		label_averageRollB.setLayoutData(gd_label_averageRollB);
		label_averageRollB.setToolTipText("Average defender roll (includes handicap!).");
		label_averageRollB.setText("muuuuh");
		label_averageRollB.setVisible(false);

		Label label_battleDuration = new Label(composite_overallResults, SWT.CENTER);
		GridData gd_label_battleDuration = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_label_battleDuration.verticalIndent = 20;
		label_battleDuration.setLayoutData(gd_label_battleDuration);
		label_battleDuration.setFont(SWTResourceManager.getFont(FONT, 12, SWT.BOLD));
		label_battleDuration.setAlignment(SWT.CENTER);
		// Preset some invisible text so layout does not set get screwed
		label_battleDuration.setText("50000 days\r\n");
		label_battleDuration.setVisible(false);

		Label label_casualties = new Label(composite_overallResults, SWT.CENTER);
		label_casualties.setFont(SWTResourceManager.getFont(FONT, 12, SWT.BOLD));
		GridData gd_label_casualties = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_label_casualties.verticalIndent = 20;
		gd_label_casualties.horizontalIndent = -1;
		label_casualties.setLayoutData(gd_label_casualties);
		label_casualties.setText("20000 dead");
		label_casualties.setVisible(false);

		ResultsComposite results_defender = new ResultsComposite(shlEuCombatSimulator, SWT.NONE);
		results_defender.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));

		Menu menu = new Menu(shlEuCombatSimulator, SWT.BAR);
		shlEuCombatSimulator.setMenuBar(menu);
		MenuItem mntmHelp = new MenuItem(menu, SWT.CASCADE);
		mntmHelp.setText("Help");
		Menu menu_cascade = new Menu(mntmHelp);
		mntmHelp.setMenu(menu_cascade);
		MenuItem mntmAbout = new MenuItem(menu_cascade, SWT.NONE);
		mntmAbout.setText("About");
		mntmAbout.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				MessageBox messageBox = new MessageBox(shlEuCombatSimulator, SWT.ICON_INFORMATION);
				messageBox.setMessage(HELP_MESSAGE);
				messageBox.open();
			}
		});

		btnSimulate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Refactor, also much duplicated code for both sides
				Army armyA = attackerGroup.getArmy();
				armyA.dieRollProvider.addHandicap(combo_attackerRoll.getSelection());
				Army armyB = defenderGroup.getArmy();
				armyB.dieRollProvider.addHandicap(combo_defenderRoll.getSelection());

				int numberOfIterations = spinner_numberOfRuns.getSelection();
				SimulationController sim = new SimulationController(numberOfIterations, armyA, armyB,
						trngrpTerrain.getTerrain(), trngrpTerrain.getPenalty(), progressBar);
				sim.run();
				List<BattleStats> statsA = sim.getResults(armyA);
				List<BattleStats> statsB = sim.getResults(armyB);
				results_attacker.update(statsA);
				results_defender.update(statsB);
				NumberFormat df = NumberFormat.getInstance();
				df.setMaximumFractionDigits(2);
				label_averageRollA.setText(df.format(armyA.dieRollProvider.averageRoll()));
				label_averageRollA.setVisible(true);
				label_averageRollB.setText(df.format(armyB.dieRollProvider.averageRoll()));
				label_averageRollB.setVisible(true);
				double totalCasualties = statsA.stream().collect(Collectors.averagingInt(BattleStats::casualties))
						+ statsB.stream().collect(Collectors.averagingInt(BattleStats::casualties));
				double averageDuration = statsA.stream().collect(Collectors.averagingInt(BattleStats::getDuration));

				// TODO Too much in this one label, make middle result composite

				label_battleDuration.setText(ResultsComposite.IF.format(averageDuration) + " days");
				label_battleDuration.setVisible(true);
				label_casualties.setText(ResultsComposite.IF.format(totalCasualties) + " dead");
				label_casualties.setVisible(true);

			}
		});

	}
}
