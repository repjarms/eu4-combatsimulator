package eu4.comsim.gui;

import java.util.stream.Stream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.datatypes.Terrain;
import eu4.comsim.core.datatypes.Terrain.CrossingPenalty;

/**
 * Contains UI elements regarding choice of {@link Terrain} and potential
 * attacker penalties from it (strait, river, landing ...)
 *
 */
public class TerrainGroup extends Group {

	private Combo combo_attackerPenalty;
	Button button_terrain;

	Terrain selectedTerrain = Terrain.FOREST;
	CrossingPenalty selectedPenalty = Terrain.CrossingPenalty.RIVER;

	/**
	 * Create the composite.
	 *
	 * @param parent
	 * @param style
	 */
	public TerrainGroup(Composite parent, int style) {
		super(parent, style);
		setText("Terrain");
		setLayout(new GridLayout(2, false));
		button_terrain = new Button(this, SWT.NONE);
		button_terrain.setToolTipText("Right-click to choose terrain");
		button_terrain.setImage(SWTResourceManager.getImage(TerrainGroup.class, selectedTerrain.imagePath));

		Menu menu_chooseTerrain = new Menu(button_terrain);
		button_terrain.setMenu(menu_chooseTerrain);
		// TODO Could refactor to create identical menus from Enum.values
		for (Terrain t : Terrain.values()) {
			MenuItem item = new MenuItem(menu_chooseTerrain, SWT.PUSH);
			item.setText(t.name);
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					button_terrain.setImage(SWTResourceManager.getImage(TerrainGroup.class,
							Terrain.fromString(item.getText()).imagePath));
					selectedTerrain = Terrain.fromString(item.getText());
				}
			});
		}
		combo_attackerPenalty = new Combo(this, SWT.READ_ONLY);
		combo_attackerPenalty
				.setItems(Stream.of(Terrain.CrossingPenalty.values()).map(t -> t.name).toArray(String[]::new));
		combo_attackerPenalty.select(selectedPenalty.ordinal());
		combo_attackerPenalty.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

	}

	public Terrain.CrossingPenalty getPenalty() {
		return Terrain.CrossingPenalty.values()[combo_attackerPenalty.getSelectionIndex()];
	}

	public Terrain getTerrain() {
		return selectedTerrain;
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
