package eu4.comsim.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.General;

/**
 * Simple Composite containing UI elements for selecting General stats.
 */
public class GeneralGroup extends Group {

    private Spinner spinner_fire;
    private Spinner spinner_maneuver;
    private Spinner spinner_shock;
    private Spinner spinner_siege;

    public GeneralGroup(Composite parent, int style, String name) {
	super(parent, style);
	setText(name);
	RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
	rowLayout.center = true;
	rowLayout.justify = true;
	rowLayout.wrap = false;
	setLayout(rowLayout);

	Label label_fire = new Label(this, SWT.NONE);
	label_fire.setToolTipText("Fire");
	label_fire.setImage(SWTResourceManager.getImage(GeneralGroup.class, "/general/Land_leader_fire.png"));

	spinner_fire = new Spinner(this, SWT.BORDER);
	spinner_fire.setLayoutData(new RowData(5, SWT.DEFAULT));
	spinner_fire.setToolTipText("Select fire pips (0-6). Applied in fire phases");
	spinner_fire.setSelection(2);

	Label label_shock = new Label(this, SWT.NONE);
	label_shock.setToolTipText("Shock");
	label_shock.setImage(SWTResourceManager.getImage(GeneralGroup.class, "/general/Land_leader_shock.png"));

	spinner_shock = new Spinner(this, SWT.BORDER);
	spinner_shock.setLayoutData(new RowData(5, SWT.DEFAULT));
	spinner_shock.setToolTipText("Select fire pips (0-6). Applied during shock phases.");
	spinner_shock.setSelection(1);

	Label label_maneuver = new Label(this, SWT.NONE);
	label_maneuver.setToolTipText("Maneuver");
	label_maneuver.setImage(SWTResourceManager.getImage(GeneralGroup.class, "/general/Land_leader_maneuver.png"));

	spinner_maneuver = new Spinner(this, SWT.BORDER);
	spinner_maneuver.setLayoutData(new RowData(5, SWT.DEFAULT));
	spinner_maneuver.setToolTipText("Select maneuver pips. May help cancel out crossing penalties.");
	spinner_maneuver.setSelection(1);

	Label label_siege = new Label(this, SWT.NONE);
	label_siege.setToolTipText("Siege");
	label_siege.setImage(SWTResourceManager.getImage(GeneralGroup.class, "/general/Leader_siege.png"));

	spinner_siege = new Spinner(this, SWT.BORDER);
	spinner_siege.setLayoutData(new RowData(5, SWT.DEFAULT));
	spinner_siege.setToolTipText("Select siege pips (0-6). Has no influence on combat.");
	spinner_siege.setSelection(1);
    }

    /**
     * @return A new {@link General} from the current spinner (fire, shock,
     *         maneuver, siege) selections
     */
    public General getGeneral() {
	return new General(spinner_fire.getSelection(), spinner_shock.getSelection(), spinner_maneuver.getSelection(),
		spinner_siege.getSelection());
    }

    @Override
    protected void checkSubclass() {
	// Disable the check that prevents subclassing of SWT components
    }
}
