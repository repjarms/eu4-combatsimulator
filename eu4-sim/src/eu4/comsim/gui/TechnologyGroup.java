package eu4.comsim.gui;

import java.text.NumberFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.Technology;

/**
 * A {@link Group} holding all information regarding a nation/army's
 * {@link Technology}, and providing input controls for the user to change
 * various aspects of it.
 */
public class TechnologyGroup extends Group {

	public static NumberFormat NF = NumberFormat.getInstance();

	private Spinner spinner_level;
	private Spinner spinner_infCA;
	private Spinner spinner_morale;
	private Spinner spinner_disc;
	private Spinner spinner_cavCA;
	private Spinner spinner_artCA;

	Technology.Group selectedTechGroup = Technology.Group.WESTERN;

	private Label label_totalMorale;
	private Label label_totalDiscipline;
	private Label label_totalTactics;

	public TechnologyGroup(Composite parent, int style, String name) {
		super(parent, style);
		NF.setMinimumFractionDigits(2);
		setLayout(new GridLayout(5, true));
		setText(name);

		Label label_level = new Label(this, SWT.CENTER);
		label_level.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 2, 1));
		label_level.setToolTipText("Military technology level");
		label_level.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Military_tech.png"));

		spinner_level = new Spinner(this, SWT.BORDER);
		spinner_level.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		spinner_level.setToolTipText("Select military tech level (1-32)");
		spinner_level.setMaximum(30);
		spinner_level.setMinimum(1);
		spinner_level.setSelection(13);

		Button button_techgroup = new Button(this, SWT.NONE);
		button_techgroup.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 2, 1));
		button_techgroup.setToolTipText("Right-click to choose technology group");
		button_techgroup.setImage(SWTResourceManager.getImage(Technology.Group.class, selectedTechGroup.imagePath));

		// TODO For all the following, it would be nice for the user to see the
		// final values, not just the modifiers
		// i.e. the final value on top of the base provided by the mil tech
		// level, like discipline 3.8
		Menu menu_chooseTG = new Menu(button_techgroup);
		button_techgroup.setMenu(menu_chooseTG);

		Label label_morale = new Label(this, SWT.NONE);
		label_morale.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		label_morale.setToolTipText("Morale");
		label_morale.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Morale.png"));

		for (Technology.Group tg : Technology.Group.values()) {
			MenuItem item = new MenuItem(menu_chooseTG, SWT.NONE);
			item.setText(tg.name);
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					selectedTechGroup = Technology.Group.fromString(item.getText());
					button_techgroup
							.setImage(SWTResourceManager.getImage(Technology.Group.class, selectedTechGroup.imagePath));
					// somewhat ugly but whatever
					((ArmyGroup) getParent()).updateCombos(getTechnology());
				}
			});
		}

		spinner_morale = new Spinner(this, SWT.BORDER);
		spinner_morale.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_morale.setToolTipText("Select morale multiplier (100 = 100% of base)");
		spinner_morale.setMaximum(500);
		spinner_morale.setSelection(100);
		spinner_morale.addModifyListener(e -> {
			label_totalMorale.setText(NF.format(getTechnology().getMorale()));
		});

		label_totalMorale = new Label(this, SWT.NONE);
		label_totalMorale.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		label_totalMorale.setToolTipText("Total morale (Base value x modifier)");

		Label label_infCA = new Label(this, SWT.NONE);
		label_infCA.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		label_infCA.setToolTipText("Infantry combat ability");
		label_infCA.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Infantry_power.png"));

		spinner_infCA = new Spinner(this, SWT.BORDER);
		spinner_infCA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_infCA.setMaximum(500);
		spinner_infCA.setSelection(100);
		spinner_infCA.setToolTipText("Select infantry combat ability modifer (100 = 100% of base)");

		Label label_discipline = new Label(this, SWT.NONE);
		label_discipline.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		label_discipline.setToolTipText("Discipline");
		label_discipline.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Discipline.png"));

		spinner_disc = new Spinner(this, SWT.BORDER);
		spinner_disc.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_disc.setToolTipText("Select discipline modifier (100 = 100% of base value)");
		spinner_disc.setMaximum(300);
		spinner_disc.setSelection(100);
		spinner_disc.addModifyListener(e -> {
			label_totalDiscipline.setText(NF.format(getTechnology().getDiscipline()));
			label_totalTactics.setText(NF.format(getTechnology().getTactics()));
		});

		label_totalDiscipline = new Label(this, SWT.NONE);
		label_totalDiscipline.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		label_totalDiscipline.setToolTipText("Total discipline (From military technology and multiplier)");

		Label label_cavCA = new Label(this, SWT.NONE);
		label_cavCA.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		label_cavCA.setToolTipText("Cavalry combat ability");
		label_cavCA.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Cavalry_power.png"));

		spinner_cavCA = new Spinner(this, SWT.BORDER);
		spinner_cavCA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_cavCA.setMaximum(500);
		spinner_cavCA.setSelection(100);
		spinner_cavCA.setToolTipText("Select cavalry combat ability modifer (100 = 100% of base)");

		Label label_tactics = new Label(this, SWT.NONE);
		label_tactics.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 2, 1));
		label_tactics.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Military_tactics.png"));
		label_tactics.setToolTipText("Military Tactics");

		label_totalTactics = new Label(this, SWT.NONE);
		label_totalTactics.setToolTipText(
				"Military tactics (Automatically determined from discipline and military technology level)");
		label_totalTactics.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));

		Label label_artCA = new Label(this, SWT.NONE);
		label_artCA.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		label_artCA.setToolTipText("Artillery combat ability");
		label_artCA.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Artillery_power.png"));

		spinner_artCA = new Spinner(this, SWT.BORDER);
		spinner_artCA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_artCA.setToolTipText("Select artillery combat ability modifer (100 = 100% of base)");
		spinner_artCA.setMaximum(500);
		spinner_artCA.setSelection(100);

		spinner_level.addModifyListener(e -> {
			label_totalMorale.setText(NF.format(getTechnology().getMorale()));
			label_totalDiscipline.setText(NF.format(getTechnology().getDiscipline()));
			label_totalTactics.setText(NF.format(getTechnology().getTactics()));
		});

		label_totalMorale.setText(NF.format(getTechnology().getMorale()));
		label_totalDiscipline.setText(NF.format(getTechnology().getDiscipline()));
		label_totalTactics.setText(NF.format(getTechnology().getTactics()));

	}

	/**
	 * @return A new {@link Technology} generated from the mil tech level (
	 *         {@link #selectedTechGroup}) and spinner values
	 */
	public Technology getTechnology() {
		return new Technology(spinner_level.getSelection(), selectedTechGroup, spinner_disc.getSelection() / 100.0,
				spinner_morale.getSelection() / 100.0, spinner_infCA.getSelection() / 100.0,
				spinner_cavCA.getSelection() / 100.0, spinner_artCA.getSelection() / 100.0);
	}

	/**
	 * Public since other UI components need to attach listeners to this
	 * control.
	 */
	public Spinner getLevelSpinner() {
		return spinner_level;
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
