package eu4.comsim.gui;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.Army;
import eu4.comsim.core.DieRollProvider;
import eu4.comsim.core.Regiment;
import eu4.comsim.core.Technology;
import eu4.comsim.core.datatypes.Unit;
import eu4.comsim.core.datatypes.UnitType;

/**
 * Composite containing all information relevant for one side of a battle.<br>
 * A {@link CombatSimulator} will contain two of these; one for the attacking
 * and one for the defending side
 */
public class ArmyGroup extends Group {

	// Comboboxes for Unit - input will automatically be updated when
	// technology group or level is changed. Initialized in constructor
	Combo[] combos_unit = new Combo[3];
	// Controls number of regiments of each Unit
	Spinner[] spinners_unit = new Spinner[3];

	// Sub composites - General and Technology
	GeneralGroup group_general;
	TechnologyGroup group_tech;

	/**
	 * Create a new {@link ArmyGroup} composite.
	 *
	 * @param name
	 *            The name used as the Group's text
	 */
	public ArmyGroup(Composite parent, int style, String name) {
		super(parent, style);
		setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		setText(name);
		setLayout(new GridLayout(1, false));

		group_tech = new TechnologyGroup(this, SWT.NONE, "Technology");
		group_tech.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		group_tech.getLevelSpinner().addModifyListener(e -> {
			Technology selectedTech = group_tech.getTechnology();
			updateCombos(selectedTech);
		});

		Group grpArmy = new Group(this, SWT.NONE);
		grpArmy.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		grpArmy.setText("Army");
		grpArmy.setLayout(new GridLayout(1, false));

		group_general = new GeneralGroup(grpArmy, SWT.NONE, "General");
		group_general.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		Group grpRegiments = new Group(grpArmy, SWT.NONE);
		grpRegiments.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		grpRegiments.setText("Regiments");
		GridLayout gl_grpRegiments = new GridLayout(3, false);
		grpRegiments.setLayout(gl_grpRegiments);

		for (UnitType type : UnitType.values()) {
			Label label_type = new Label(grpRegiments, SWT.NONE);
			label_type.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
			label_type.setToolTipText(type.name);
			label_type.setImage(SWTResourceManager.getImage(CombatSimulator.class, type.imagePath));

			Combo combo_type = new Combo(grpRegiments, SWT.READ_ONLY);
			combo_type.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			fillCombo(combo_type, group_tech.getTechnology(), type);
			// TODO Not that beautiful to have a mouse track listener here for
			// the tooltip
			combo_type.addModifyListener(e -> {
				String unit = combo_type.getItem(0);
				String trimmed = (unit.length() > 0) ? unit.substring(unit.indexOf(" ") + 1) : "No unit selected";
				combo_type.setToolTipText(Unit.fromString(trimmed).getTooltip());
			});

			combos_unit[type.id] = combo_type;

			Spinner spinner_type = new Spinner(grpRegiments, SWT.BORDER);
			spinner_type.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			spinner_type.setSelection(type.getDefaultBataillonSize());
			spinners_unit[type.id] = spinner_type;
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * @return Reads the value of the {@link #combos_unit} boxes and creates a
	 *         new set of regiments from it
	 */
	public List<Regiment> getRegiments() {

		@SuppressWarnings("serial")
		Map<Unit, Integer> boilerPlate = new Hashtable<Unit, Integer>() {
			{
				for (int i = 0; i < combos_unit.length; i++) {
					// "[10] Schwarze Reiter" -> "Schwarze Reiter"
					String unit = combos_unit[i].getText();
					String trimmed = unit.substring(unit.indexOf(" ") + 1);
					if (trimmed.length() > 0) {
						// Catch empty text (for example, no artillery below lvl
						// 7)
						put(Unit.fromString(trimmed), spinners_unit[i].getSelection());
					}
				}
			}
		};
		return Regiment.createRegiments(group_tech.getTechnology(), boilerPlate);
	}

	/**
	 * @return Collects general and regiments for all the input, and builds an
	 *         {@link Army} from it.
	 */
	public Army getArmy() {
		return new Army(getGeneralGroup().getGeneral(), getTechnologyGroup().getTechnology(), getRegiments(),
				DieRollProvider.random());
	}

	/**
	 * Exposes the child control {@link TechnologyGroup} to clients
	 *
	 * @return the {@link TechnologyGroup}
	 */
	public TechnologyGroup getTechnologyGroup() {
		return group_tech;
	}

	/**
	 * Exposes the child control {@link GeneralGroup} to clients
	 *
	 * @return the {@link GeneralGroup}
	 */
	public GeneralGroup getGeneralGroup() {
		return group_general;
	}

	/**
	 * The values of all the Unit combo boxes are updated
	 *
	 * @param selectedTech
	 *            provides the mil tech level and tech group the combo items
	 *            have to match afterwards
	 */
	public void updateCombos(Technology selectedTech) {
		for (int i = 0; i < combos_unit.length; i++) {
			fillCombo(combos_unit[i], selectedTech, UnitType.values()[i]);
		}
	}

	/**
	 * Fills a {@link Combo} with {@link Unit}s of the given {@link UnitType}
	 * available at the given {@link Technology} level<br>
	 * The results will be sorted by tech level in descending order, so the most
	 * recent units are on top.
	 */
	public static void fillCombo(Combo combo, Technology t, UnitType type) {
		// Predicate: Same UnitType, techlevel of Unit does not exceed mil tech
		// level reached and techgroup must match (for non-artillery)
		Predicate<? super Unit> isSelectable = u -> (u.type == type && u.tech_level <= t.getLevel()
				&& ((u.type != UnitType.ARTILLERY) ? u.tech_group == t.getGroup() : true));
		// Return all units that match the selection criteria, sort them
		// descending by tech level
		Stream<Unit> eligible = Stream.of(Unit.values())
				.filter(isSelectable)
				.sorted((u1, u2) -> Integer.compare(u2.tech_level, u1.tech_level));
		String[] names = eligible.map(u -> "[" + Integer.toString(u.tech_level) + "] " + u.getName())
				.toArray(String[]::new);
		combo.setItems(names);
		combo.setText(combo.getItem(0));
		if (names.length > 0) {
			String unit = combo.getText();
			String trimmed = (unit.length() > 0) ? unit.substring(unit.indexOf(" ") + 1) : "No unit selected";

			combo.setToolTipText(Unit.fromString(trimmed).getTooltip());
		}
	}

}
