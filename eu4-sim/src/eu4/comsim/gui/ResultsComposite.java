package eu4.comsim.gui;

import java.text.NumberFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.BattleStats;
import eu4.comsim.core.datatypes.UnitType;

/**
 * Displays results of a combat simulation.
 */
public class ResultsComposite extends Composite {

    private static final NumberFormat DF = NumberFormat.getInstance();
    private static final NumberFormat PF = NumberFormat.getPercentInstance();
    public static NumberFormat IF = NumberFormat.getIntegerInstance();

    /**
     * A table displaying casualties, routed and fighting men, as well as
     * percentage information
     */
    Table table;
    private Label label_winningPct;
    private Label label_changeToLastSim;

    private double casualties;
    private double routed;
    private double averageMorale;
    private double fighting;

    private double originalStrength;
    private double winningPct;
    private double lastWinningPct;

    /**
     * Used to save the results of the previous simulation run. Written to when
     * {@link #update(List)} is called.
     */
    List<BattleStats> previousRunStats;

    /**
     * Create the composite and initialize its components
     */
    public ResultsComposite(Composite parent, int style) {
	super(parent, style);
	setToolTipText("Change to last simulation");
	setLayout(new GridLayout(2, true));

	label_winningPct = new Label(this, SWT.NONE);
	label_winningPct.setToolTipText("Winning percentage");
	label_winningPct.setFont(SWTResourceManager.getFont("Segoe UI", 18, SWT.BOLD));
	label_winningPct.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
	label_winningPct.setText("WINS");
	label_winningPct.setVisible(false);

	label_changeToLastSim = new Label(this, SWT.NONE);
	label_changeToLastSim.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
	label_changeToLastSim.setToolTipText("Change to last simulation");
	label_changeToLastSim.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
	label_changeToLastSim.setText("No results yet");
	label_changeToLastSim.setVisible(false);

	table = new Table(this, SWT.BORDER | SWT.FULL_SELECTION);
	GridData gd_table = new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1);
	gd_table.widthHint = 334;
	table.setLayoutData(gd_table);
	table.setHeaderVisible(true);

	TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
	tblclmnNewColumn.setWidth(62);
	TableColumn tblclmnCasualties = new TableColumn(table, SWT.NONE);
	tblclmnCasualties.setWidth(67);
	tblclmnCasualties.setText("Casualties");

	TableColumn tblclmnRouted = new TableColumn(table, SWT.NONE);
	tblclmnRouted.setWidth(61);
	tblclmnRouted.setText("Routed");

	for (UnitType type : UnitType.values()) {
	    TableItem tableItem = new TableItem(table, SWT.NONE);
	    tableItem.setText(type.name);
	}

	TableItem tableItem_Total = new TableItem(table, SWT.NONE);
	tableItem_Total.setText("Total");

	TableColumn tblclmnFighting = new TableColumn(table, SWT.NONE);
	tblclmnFighting.setWidth(161);
	tblclmnFighting.setText("Remaining (avg. morale)");

	TableItem tableItem_Percent = new TableItem(table, SWT.NONE);
	tableItem_Percent.setText("Percent");
    }

    // TODO Prettify, so much duplicated code, formatting issues etc
    void fillTable(List<BattleStats> list) {
	DF.setMinimumFractionDigits(2);
	DF.setMaximumFractionDigits(2);

	for (int i = 0; i <= 2; i++) {
	    UnitType type = UnitType.values()[i];
	    double casualtiesForType = list.stream()
		    .map(b -> b.adaptTo(type))
		    .collect(Collectors.averagingInt(BattleStats::casualties));
	    double routedForType = list.stream()
		    .map(b -> b.adaptTo(type))
		    .collect(Collectors.averagingInt(BattleStats::routed));
	    double fightingForType = list.stream()
		    .map(b -> b.adaptTo(type))
		    .collect(Collectors.averagingInt(BattleStats::fighting));
	    double averageMoraleForType = list.stream()
		    .map(b -> b.adaptTo(type))
		    .collect(Collectors.averagingDouble(BattleStats::averageMorale));
	    table.getItem(i).setText(1, IF.format(casualtiesForType));
	    table.getItem(i).setText(2, IF.format(routedForType));
	    table.getItem(i).setText(3, IF.format(fightingForType) + " (" + DF.format(averageMoraleForType) + ")");
	}

	table.getItem(3).setText(1, IF.format(casualties));
	table.getItem(3).setText(2, IF.format(routed));
	table.getItem(3).setText(3, IF.format(fighting) + " (" + DF.format(averageMorale) + ")");

	table.getItem(4).setText(1, PF.format(casualties / originalStrength));
	table.getItem(4).setText(2, PF.format(routed / originalStrength));
	table.getItem(4).setText(3, PF.format(fighting / originalStrength));
    }

    public void update(List<BattleStats> list) {
	casualties = list.stream().collect(Collectors.averagingInt(BattleStats::casualties));
	routed = list.stream().collect(Collectors.averagingInt(BattleStats::routed));
	averageMorale = list.stream().collect(Collectors.averagingDouble(BattleStats::averageMorale));
	fighting = list.stream().collect(Collectors.averagingInt(BattleStats::fighting));
	originalStrength = list.stream().collect(Collectors.averagingInt(BattleStats::originalStrength));

	winningPct = (double) list.stream().filter(s -> s.fighting() > 0).count() / (double) list.size();
	label_winningPct.setText(PF.format(winningPct));
	int colorId = (winningPct > 0.5) ? SWT.COLOR_DARK_GREEN : SWT.COLOR_DARK_RED;
	label_winningPct.setForeground(SWTResourceManager.getColor(colorId));
	// TODO showing the comparison to the last run
	label_winningPct.setVisible(true);

	if (previousRunStats != null) {
	    double change = winningPct - lastWinningPct;
	    String plus = (change > 0) ? "+" : "";
	    label_changeToLastSim.setText("(" + plus + PF.format(change) + ")");
	    colorId = (change > 0) ? SWT.COLOR_DARK_GREEN : SWT.COLOR_DARK_RED;
	    label_changeToLastSim.setForeground(SWTResourceManager.getColor(colorId));
	    // TODO showing the comparison to the last run
	    label_changeToLastSim.setVisible(true);

	}

	fillTable(list);
	lastWinningPct = winningPct;
	previousRunStats = list;

    }

}
