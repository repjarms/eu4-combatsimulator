package eu4.comsim.core;

import java.util.List;

public class Army {

    /** technology level of the nation the army belongs to */
    public final Technology technology;
    /** The general leading the army */
    public final General general;
    /**
     * The regiments the army is composed of. The only part of the army that
     * changes during the simulation of a {@link Battle}
     */
    public final List<Regiment> regiments;
    /**
     * Provides the (random or not so random) die rolls this army uses during
     * combat
     */
    public final DieRollProvider dieRollProvider;

    /**
     * An EU4 army. It holds no concrete deployment information, for this, see
     * {@link Battleline}, which accepts two armies in its constructor.
     * <p>
     * All parameters passed to this constructor are put into final fields, so
     * you are not supposed to "reuse" an Army but have to create a new one. You
     * can simulate the same battle again by calling {@link Regiment#reset()}
     * for all its regiments.
     *
     * @param general
     *            The general leading the army
     * @param technology
     *            technology level of the nation the army belongs to.
     * @param regiments
     *            the regiments the army is composed of. The only part of the
     *            army that changes during the simulation of a {@link Battle}
     * @param imFeelinLucky
     *            Provides the (random or not so random) die rolls this army
     *            uses during combat
     */
    public Army(General general, Technology technology, List<Regiment> regiments, DieRollProvider imFeelinLucky) {
	this.regiments = regiments;
	this.general = general;
	this.technology = technology;
	this.dieRollProvider = imFeelinLucky;
    }

    /**
     * All damage buffered in the army's regiments is applied, actually reducing
     * their strength and morale. Works in tandem with
     * {@link Regiment#storeCasualties(int)} and
     * {@link Regiment#storeMorale(double)}, since damage cannot be applied
     * straight away until opposing army have buffered their damage (in order to
     * prevent the starting army to have an advantage)
     */
    public void applyBufferedDamage() {
	regiments.stream().forEach(Regiment::applyDamage);
    }

    /**
     * @return <code>true</code> if all regiments are routed (morale <= 0),
     *         <code>false</code> otherwise
     */
    public boolean defeated() {
	return regiments.stream().allMatch(Regiment::routed);
    }
}