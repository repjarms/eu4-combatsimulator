package eu4.comsim.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import eu4.comsim.core.datatypes.Phase;
import eu4.comsim.core.datatypes.UnitType;

/**
 * Represents the forces actually deployed on the field during a battle
 *
 */
public class Battleline {

	// Variables that control during deployment how much cavalry spaces are
	// reserved on the side
	private static int SIDE_RESERVED_FRONTROW = 2;
	private static int SIDE_RESERVED_BACKROW = 2;
	/**
	 * combat width (influenced by mil tech level, modifiers, and opponent)
	 */
	private int width;
	private double maxCavSupportRatio;
	public List<Regiment> frontRow, backRow;
	public Collection<Regiment> allRegiments;

	private Iterator<Regiment> infantry;
	private Iterator<Regiment> cavalry;
	private Iterator<Regiment> artillery;

	/**
	 * Creates a battleline.
	 *
	 * @param regiments
	 *            Regiments to be deployed. These are placed in queues, and are
	 *            deployed initially until there is no space left. Any remaining
	 *            regiments are left in the queue and are attempted to be placed
	 *            on subsequent calls to {@link #deploy()}
	 * @param combatWidth
	 *            Combat width used for the battleline (based on mil tech of
	 *            this and opposing army)
	 * @param maxCavRatio
	 *            Maximum cavalry ratio (based on tech group)
	 */
	public Battleline(Collection<Regiment> regiments, int combatWidth, double maxCavRatio) {
		this.width = combatWidth;
		this.maxCavSupportRatio = maxCavRatio;

		frontRow = new ArrayList<>(Collections.nCopies(width, Regiment.EMPTY));
		backRow = new ArrayList<>(Collections.nCopies(width, Regiment.EMPTY));
		this.allRegiments = regiments;

		infantry = Regiment.getRegiments(regiments, UnitType.INFANTRY).iterator();
		cavalry = Regiment.getRegiments(regiments, UnitType.CAVALRY).iterator();
		artillery = Regiment.getRegiments(regiments, UnitType.ARTILLERY).iterator();

		deploy();
	}

	/**
	 * @param defender
	 * @param phase
	 * @param roll
	 */
	public void attackAll(Battleline defender, Phase phase, int roll) {
		double cavSupportPenalty = defender.hasInsufficientSupport() ? defender.maxCavSupportRatio : 1.0;
		for (Regiment attackingRegiment : allActiveRegiments(allRegiments)) {
			int position = frontRow.contains(attackingRegiment) ? frontRow.indexOf(attackingRegiment)
					: backRow.indexOf(attackingRegiment);
			Regiment defendingRegiment = defender.findTarget(position, attackingRegiment.getFlankingRange());
			if (defendingRegiment == Regiment.EMPTY) { return; }
			Regiment behind = defender.backRow.get(defender.frontRow.indexOf(defendingRegiment));
			double offArtMult = attackingRegiment.isOfType(UnitType.ARTILLERY) && backRow.contains(attackingRegiment)
					? 0.5 : 1;
			double defArtMult = defendingRegiment.isOfType(UnitType.ARTILLERY) ? 2 : 1;
			double totalMult = cavSupportPenalty * offArtMult * defArtMult;
			attackingRegiment.attack(defendingRegiment, phase, roll, behind, totalMult);
		}

	}

	/**
	 * Target finding routine <br>
	 * TODO Determine selection heuristics for multiple legal targets and
	 * implement it
	 *
	 * @param position
	 *            index representing a position in the battle line.
	 * @param flankingRange
	 *            maximum difference between positions the unit can target.
	 * @return the target regiment if there is an eligible target,
	 *         <code>null</code> otherwise
	 */
	public Regiment findTarget(int position, int flankingRange) {
		Comparator<Regiment> nearestToPosition = Comparator.comparingInt(r -> Math.abs(frontRow.indexOf(r) - position));
		Regiment result = frontRow.stream()
				.filter(Regiment.IS_EMPTY.negate())
				.sorted(nearestToPosition)
				.findFirst()
				.orElse(Regiment.EMPTY);
		return result;
	}

	/**
	 * <li>Removes regiments whose morale has been reduced to zero from its
	 * position
	 * <li>{@link #backRow} units move to the {@link #frontRow} when frontRow is
	 * empty <br>
	 * <li>Attempt to fill empty positions with unused regiments from queue<br>
	 *
	 */
	public void consolidate() {
		// Remove all routed regiments
		frontRow.replaceAll(r -> (r.routed() ? Regiment.EMPTY : r));
		backRow.replaceAll(r -> (r.routed() ? Regiment.EMPTY : r));
		// Shuffle up back row
		// TODO Determine: Do sometimes frontRow units shuffle over in empty
		// spaces instead of pulling the backRow to the front?
		for (int i = 0; i < frontRow.size(); i++) {
			if (frontRow.get(i) == Regiment.EMPTY) {
				frontRow.set(i, backRow.get(i));
				backRow.set(i, Regiment.EMPTY);
			}
		}
		// Attempt to fill empty positions with unused regiments from queue
		// (these have taken morale damage while standing by!)
		deploy();
	}

	/**
	 * Legacy console output code. Refactor to use logging framework
	 */
	public void print() {
		for (int i = 0; i < frontRow.size() + 1; i++) {
			System.out.print("--------");
		}
		System.out.println();
		frontRow.forEach(r -> System.out.print(r + "\t"));
		System.out.println();
		backRow.forEach(r -> System.out.print(r + "\t"));
		System.out.println();
	}

	/**
	 * @return Regiments with morale > 0
	 */
	public static Collection<Regiment> allActiveRegiments(Collection<Regiment> regiments) {
		return regiments.stream().filter(r -> !r.routed()).collect(Collectors.toList());
	}

	/**
	 * @return <code>true</code> if combined strength of cavalry (individual
	 *         soldiers) percentage of total strength exceeds the
	 *         {@link #maxCavSupportRatio}, <code>false</code> otherwise
	 */
	public boolean hasInsufficientSupport() {

		int total = allRegiments.stream().mapToInt(Regiment::getStrength).sum();
		int cav = allRegiments.stream().filter(r -> r.isOfType(UnitType.CAVALRY)).mapToInt(Regiment::getStrength).sum();
		return maxCavSupportRatio > (double) cav / (double) total;
	}

	/**
	 * Deploys available Regiments to empty battlefield locations.
	 *
	 * @see <a href="http://www.eu4wiki.com/Land_warfare#Unit_deployment">http:/
	 *      /www.eu4wiki.com/Land_warfare#Unit_deployment</a>
	 */
	public void deploy() {

		while (hasSpace(frontRow) && (cavalry.hasNext() || infantry.hasNext())) {
			Optional<Integer> nearestEmptyPosition = getNearestOpenPosition(frontRow);
			if (nearestEmptyPosition.isPresent()) {
				int position = nearestEmptyPosition.get();
				boolean outside = position < SIDE_RESERVED_FRONTROW || position > (width - SIDE_RESERVED_FRONTROW);
				Regiment r = (infantry.hasNext() && !outside) ? infantry.next()
						: cavalry.hasNext() ? cavalry.next() : infantry.next();
				frontRow.set(position, r);
			}
		}
		while (hasSpace(backRow) && (artillery.hasNext() || infantry.hasNext() || cavalry.hasNext())) {
			Optional<Integer> nearestEmptyPosition = getNearestOpenPosition(backRow);
			if (nearestEmptyPosition.isPresent()) {
				int position = nearestEmptyPosition.get();
				boolean outside = position < SIDE_RESERVED_BACKROW || position > (width - SIDE_RESERVED_BACKROW);
				if (outside && artillery.hasNext() && hasSpace(frontRow)) {
					frontRow.set(getNearestOpenPosition(frontRow).get(), artillery.next());
					continue;
				}
				Regiment r = artillery.hasNext() ? artillery.next()
						: infantry.hasNext() ? infantry.next() : cavalry.next();
				backRow.set(nearestEmptyPosition.get(), r);

			}
		}
	}

	private Optional<Integer> getNearestOpenPosition(List<Regiment> row) {
		int middle = width / 2;
		Comparator<Integer> distanceToMiddle = Comparator.comparingInt(r -> Math.abs(r - middle));
		Optional<Integer> nearestEmptyPosition = IntStream.range(0, row.size())
				.boxed()
				.filter(i -> Regiment.IS_EMPTY.test(row.get(i)))
				.collect(Collectors.minBy(distanceToMiddle));
		return nearestEmptyPosition;

	}

	private boolean hasSpace(List<Regiment> row) {
		return getNearestOpenPosition(row).isPresent();
	}
}
