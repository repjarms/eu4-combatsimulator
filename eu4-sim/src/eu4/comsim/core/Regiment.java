package eu4.comsim.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import eu4.comsim.core.datatypes.CombatType;
import eu4.comsim.core.datatypes.Phase;
import eu4.comsim.core.datatypes.Unit;
import eu4.comsim.core.datatypes.UnitType;

/**
 * An individual regiment in an {@link Army} or {@link Battleline}
 *
 */
public class Regiment {

    /** Base strength of a regiment */
    public static final int FULL_STRENGTH = 1000;

    /**
     * Static identifier for an "empty" regiment, signifying the corresponding
     * place in the battlefield is empty
     */
    public static final Regiment EMPTY = new Regiment(-1, Unit.HALBERD_INFANTRY, 0, new Technology(1));
    private int strength;
    private double currentMorale;

    private int bufferedCasualties;
    private double bufferedMorale;

    Unit unit;
    Technology tech;

    // Maybe use name and id (not needed for now)
    private int id;

    /**
     * A new regiment is created out of thin air.
     *
     * @param id
     *            Optional identifier.
     * @param unit
     *            Unit of the regiment.
     * @param strength
     *            Number of individual soldiers, in almost all cases use
     *            {@value #FULL_STRENGTH}
     * @param tech
     *            Technology of the regiments nation, influences morale,
     *            flanking range, damage etc.
     */
    public Regiment(int id, Unit unit, int strength, Technology tech) {
	this.id = id;
	this.unit = unit;
	this.strength = strength;
	this.tech = tech;
	this.currentMorale = tech.getMorale();
	bufferedCasualties = 0;
	bufferedMorale = 0;
    }

    /**
     * Attack of individual regiment against opponent regiment
     */
    public void attack(Regiment opponent, Phase phase, int baseRoll, Regiment support, double modifier) {
	double supportingDefensivePips = support.isOfType(UnitType.ARTILLERY)
		? support.unit.getPips(phase, CombatType.DEFENSE) : 0;
	// TODO Determine if defending artillery defensive pip contribution is
	// rounded to nearest int?
	int damageRoll = baseRoll + unit.getPips(phase, CombatType.OFFENSE)
		- opponent.unit.getPips(phase, CombatType.DEFENSE) - (int) supportingDefensivePips;
	int moraleDamageRoll = baseRoll + unit.getPips(Phase.MORALE, CombatType.OFFENSE)
		- opponent.unit.getPips(Phase.MORALE, CombatType.DEFENSE);
	int casualties = (int) (baseCasualties(damageRoll) * modifier * tech.casualtyMultiplier(unit.type, phase)
		/ opponent.tech.getTactics());
	double moraleDamage = baseCasualties(moraleDamageRoll) / 600 * modifier
		* tech.casualtyMultiplier(unit.type, Phase.MORALE) / opponent.tech.getTactics();
	opponent.storeCasualties(casualties);
	opponent.storeMorale(moraleDamage);
    }

    /**
     * Changes internal state by applying all buffered changes from
     * {@link #storeCasualties(int)} and {@link #storeMorale(double)}
     */
    public void applyDamage() {
	strength = Math.max(0, strength - bufferedCasualties);
	currentMorale = currentMorale - bufferedMorale;
	bufferedMorale = 0;
	bufferedCasualties = 0;
    }

    /**
     * @return Current number of warm bodies in the regiment
     */
    public int getStrength() {
	return strength;
    }

    /**
     * @return Current morale
     */
    public double getCurrentMorale() {
	return (strength == 0) ? 0 : Double.max(0, currentMorale);
    }

    /**
     * @return <code>true</code> if morale is above <code>0</code>,
     *         <code>false</code> otherwise
     */
    public boolean routed() {
	return !(getCurrentMorale() > 0);
    }

    /**
     * Changes morale. <br>
     *
     * Used for example
     * <li>(+0.5) at the start of a battle for the attacker
     * <li>(-0.01) after each day regardless if casualties were taken
     *
     *
     * @param value
     *            The value to change.
     */
    public void changeMorale(double value) {
	currentMorale += value;
    }

    private double baseCasualties(int roll) {
	double base = (15 + 5 * roll) * strength / 1000.0;
	// Here, make sure we never have negative casualties (could happen if
	// large discrepancies in all contributing factors on one side (general,
	// unit, terrain, handicap, random roll)
	base = Math.max(0, base);
	return base;
    }

    private void storeCasualties(int casualties) {
	bufferedCasualties += casualties;
    }

    private void storeMorale(double morale) {
	bufferedMorale += morale;
    }

    /**
     * @return Flanking range of the regiment, depends on its current
     *         {@link #strength}
     */
    public int getFlankingRange() {
	double flankingMod = 1.0;
	if (strength < 750) {
	    flankingMod = 0.75;
	}
	if (strength < 500) {
	    flankingMod = 0.5;
	}
	if (strength < 250) {
	    flankingMod = 0.25;
	}
	return (int) (unit.type.flankingRange * tech.getFlankingRange() * flankingMod);
    }

    /**
     * Sets strength to 0 (happens specifically when morale reaches 0 before day
     * 12). Opposite of {@link #reset()}.
     *
     * @see #reset()
     */
    public void wipe() {
	strength = 0;
	currentMorale = 0;
    }

    /**
     * Resets strength and morale to default (pre-Battle) values. Opposite of
     * {@link #wipe()}.
     *
     * @see #wipe()
     */
    public void reset() {
	strength = FULL_STRENGTH;
	currentMorale = tech.getMorale();
    }

    /**
     * Useful for saving a regiment for stats
     *
     * @return Regiment, with all values reset (morale, strength, ...)
     */
    public Regiment copy() {
	Regiment r = new Regiment(id, unit, strength, tech);
	r.currentMorale = currentMorale;
	return r;
    }

    /**
     * Check unit type
     */
    public boolean isOfType(UnitType type) {
	return (unit.type == type);
    }

    /**
     * Utility method to filter regiments of given type from a source
     * collection.
     */
    public static Collection<Regiment> getRegiments(Collection<Regiment> regiments, UnitType type) {
	return regiments.stream().filter(r -> r.isOfType(type)).collect(Collectors.toList());
    }

    @Override
    public String toString() {
	// TODO This is just a shorthand for printing a Battleline to Console,
	// refactor ..
	String format = "{%04d}";
	if (isOfType(UnitType.ARTILLERY)) {
	    format = "(%04d)";
	} else if (isOfType(UnitType.INFANTRY)) {
	    format = "[%04d]";
	}
	return String.format(format, getStrength());
    }

    /**
     * Creates full-strength regiments with the given self-explanatory
     * parameters. backRow is set to <code>false</code> initially.
     */
    public static final List<Regiment> createRegiments(Technology tech, Map<Unit, Integer> ratio) {
	List<Regiment> regiments = new ArrayList<Regiment>();
	for (Unit unit : ratio.keySet()) {
	    for (int i = 0; i < ratio.get(unit); i++) {
		regiments.add(new Regiment(regiments.size(), unit, FULL_STRENGTH, tech));
	    }
	}
	return regiments;
    }

    /**
     * Predicate checking for EMPTY regiment (the constant representing empty
     * battlefield positions, not actual regiments whose strength has been
     * reduced to zero.
     */
    public static final Predicate<Regiment> IS_EMPTY = (r -> r == Regiment.EMPTY);

}
