package eu4.comsim.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.ProgressBar;

import eu4.comsim.core.datatypes.Terrain;
import eu4.comsim.core.datatypes.Terrain.CrossingPenalty;

/**
 * Controller class for running a series of battles.
 * <p>
 * Call {@link #run()}, and then retrieve results with {@link #getResults(Army)}
 */
public class SimulationController implements Runnable {
    private int iterations;

    // Keep all individual stats so we can do statistical analysis later
    List<BattleStats> overallStatsA = new ArrayList<>();
    List<BattleStats> overallStatsB = new ArrayList<>();

    private Terrain terrain;
    private CrossingPenalty crossingPenalty;

    private Army attacker;
    private Army defender;
    private ProgressBar progressBar;

    /**
     * Simulation controller that has all necessary parameters for successful
     * execution of combat
     *
     * @param progressBar
     */
    public SimulationController(int iterations, Army attacker, Army defender, Terrain terrain,
	    CrossingPenalty crossingPenalty, ProgressBar progressBar) {
	this.iterations = iterations;
	this.attacker = attacker;
	this.defender = defender;
	this.terrain = terrain;
	this.crossingPenalty = crossingPenalty;
	this.progressBar = progressBar;

    }

    /**
     * Creates {@link #iterations} number of Battles, runs them and writes the
     * stats to the relevant variables, where they can be retrieved via
     * {@link #getResults(Army)}
     *
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
	progressBar.setVisible(true);
	progressBar.setMaximum(iterations);
	for (int i = 0; i < iterations; i++) {
	    progressBar.setSelection(i);
	    Battle battle = new Battle(attacker, defender, terrain, crossingPenalty);
	    battle.run();

	    overallStatsA.add(new BattleStats(attacker.regiments, battle.getDuration(), true));
	    overallStatsB.add(new BattleStats(defender.regiments, battle.getDuration(), true));
	    // All regiments need to be reset
	    attacker.regiments.forEach(r -> r.reset());
	    defender.regiments.forEach(r -> r.reset());
	}
	progressBar.setSelection(0);
	progressBar.setVisible(false);

    }

    /**
     * Get results for the given army.
     *
     * @param army
     *            Should be one of {@link #attacker} or {@link #defender}
     */
    public List<BattleStats> getResults(Army army) {
	return (army.equals(attacker)) ? overallStatsA : overallStatsB;
    }
}
