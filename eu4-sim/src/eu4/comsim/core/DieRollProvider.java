package eu4.comsim.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * A die roll provider.
 */
public class DieRollProvider {

    private int handicap = 0;
    private List<Integer> rolls = new ArrayList<Integer>();
    private Iterator<Integer> iterator;

    private final Random rng = new Random();

    /**
     * @return A DieRollProvider that returns random rolls in the range (0-9)
     */
    public static final DieRollProvider random() {
	return DieRollProvider.from(new ArrayList<>());
    }

    /**
     * Create from a given list of integers. They all need to be in range (0-9).
     */
    public static final DieRollProvider from(List<Integer> source) {
	assert (source.stream().mapToInt(Integer::intValue).allMatch(i -> i >= 0 && i <= 9));
	DieRollProvider drp = new DieRollProvider();
	drp.iterator = source.iterator();
	return drp;
    }

    /**
     * @return The average of all dice rolls made (i.e. all calls to
     *         {@link #getNextRoll()} are stored internally)
     */
    public double averageRoll() {
	return rolls.stream().mapToInt(Integer::intValue).average().orElse(0.0);
    }

    /**
     * @return Next roll (0-9)
     */
    public int getNextRoll() {
	int roll = iterator.hasNext() ? iterator.next() : rng.nextInt(10);
	roll += handicap;
	rolls.add(roll);
	return roll;
    }

    /**
     * Adds a handicap to the die rolls
     *
     * @param value
     *            The handicap value. Positive values are added to all rolls of
     *            this General (up to a maximum of 15), while negative ones are
     *            subtracted(minimum of 0).
     *
     */
    public void addHandicap(int value) {
	this.handicap = value;
    }
}
