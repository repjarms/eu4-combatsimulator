package eu4.comsim.core;

import eu4.comsim.core.datatypes.Phase;
import eu4.comsim.core.datatypes.Terrain;
import eu4.comsim.core.datatypes.Terrain.CrossingPenalty;

/**
 * A Battle between two Armies.
 * <p>
 * Usage:</br>
 * <code>Battle b = new Battle(attacker, defender,
 * terrain, crossingPenalty)<br> b.run() <br>b.getStats()</code>
 *
 */
public class Battle implements Runnable {

    // TODO Need outside confirmation for these three crucial values!
    private static final double ATTACKER_MORALE_BONUS = 0.5;
    private static final double DAILY_MORALE_CHANGE = -0.01;
    private static final int EARLIEST_RETREAT = 12;

    Army armyA;
    Army armyB;

    Battleline battleLineA;
    Battleline battleLineB;

    Terrain terrain;
    CrossingPenalty crossingPenalty;
    int day;

    /**
     * Sets up a battle between two armies. Respective {@link Battleline}s are
     * created and initialized.
     */
    public Battle(Army attacker, Army defender, Terrain terrain, CrossingPenalty crossingPenalty) {
	armyA = attacker;
	armyB = defender;
	// Effective combat width is the maximum of the individual widths
	// according to wiki
	int effectiveCombatWidth = Math.max(attacker.technology.getCombatWidth(), defender.technology.getCombatWidth());
	battleLineA = new Battleline(attacker.regiments, effectiveCombatWidth,
		attacker.technology.getGroup().maxCavalryRatio);
	// Attacker gets +0.5 morale
	armyA.regiments.forEach(r -> r.changeMorale(ATTACKER_MORALE_BONUS));
	battleLineB = new Battleline(defender.regiments, effectiveCombatWidth,
		defender.technology.getGroup().maxCavalryRatio);

	this.terrain = terrain;
	this.crossingPenalty = crossingPenalty;
    }

    /**
     * Executes a battle <br>
     * TODO Refactor for more fine-grained control (i.e. step(), done()) to
     * allow simulation of individual days <br>
     * TODO Refactor to avoid doubled code (the same mirrored actions for both
     * sides ...)
     *
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
	day = 0;
	int rollA = 0, rollB = 0;
	while (!armyA.defeated() && !armyB.defeated()) {
	    Phase phase = getPhase(day);

	    if ((day++) % 3 == 0) {// checks modulo, then increments
		int environmentPenalty = terrain.attackerPenalty + crossingPenalty.penalty;
		rollA = armyA.dieRollProvider.getNextRoll()
			+ armyA.general.roll(armyB.general, phase, environmentPenalty);
		rollB = armyB.dieRollProvider.getNextRoll() + armyB.general.roll(armyA.general, phase, 0);
	    }

	    battleLineA.attackAll(battleLineB, phase, rollA);
	    battleLineB.attackAll(battleLineA, phase, rollB);
	    // Damage is not applied instantly but only if all casualties
	    // have been computed in the day (using the original regiment
	    // strengths)

	    armyA.applyBufferedDamage();
	    armyB.applyBufferedDamage();

	    armyA.regiments.forEach(r -> r.changeMorale(DAILY_MORALE_CHANGE));
	    armyB.regiments.forEach(r -> r.changeMorale(DAILY_MORALE_CHANGE));

	    // game doesnt' change regiment position or allow retreats for 12
	    // days
	    if (day < EARLIEST_RETREAT) {
		// Kill all regiments with 0 morale
		armyA.regiments.stream().filter(Regiment::routed).forEach(Regiment::wipe);
		armyB.regiments.stream().filter(Regiment::routed).forEach(Regiment::wipe);
	    } else {
		battleLineA.consolidate();
		battleLineB.consolidate();
	    }
	}

    }

    /**
     * days (0,1,2) -> FIRE<br>
     * days (3,4,5) -> SHOCK<br>
     * days (6,7,8) -> FIRE<br>
     * ...
     *
     * @return Phase corresponding to day
     */
    private static Phase getPhase(int day) {
	// phase is a six day cycle.
	return ((day % 6) / 3 == 0) ? Phase.FIRE : Phase.SHOCK;
    }

    /**
     * @return Duration of the battle
     */
    public int getDuration() {
	return day;
    }

}
