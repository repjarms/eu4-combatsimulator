package eu4.comsim.core.datatypes;

/**
 * Possible types of units in EU4
 */
public enum UnitType {

    INFANTRY(0, "Infantry", 1, "/general/Infantry.png"),
    CAVALRY(1, "Cavalry", 2, "/general/Cavalry.png"),
    ARTILLERY(2, "Artillery", 2, "/general/Artillery.png");

    public final int id;
    /** Human-readable name of the unit type */
    public final String name;
    /**
     * Base value how far diagonally the unit can attack opponent regiments
     * (modified by tech level)
     */
    public final int flankingRange;
    /** Classpath-relative image icon */
    public final String imagePath;

    private UnitType(int id, String name, int flankingRange, String image) {
	this.name = name;
	this.id = id;
	this.flankingRange = flankingRange;
	this.imagePath = image;
    }

    /**
     * Convenience method to create a "default" army, like an army template
     * ingame ... Will return (10 Inf - 4 Cav - 6 Art)
     */
    public int getDefaultBataillonSize() {
	switch (this) {
	    case INFANTRY:
		return 16;
	    case CAVALRY:
		return 4;
	    case ARTILLERY:
		return 10;
	    default:
		return 0;
	}
    }
}
