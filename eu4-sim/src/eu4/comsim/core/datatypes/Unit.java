package eu4.comsim.core.datatypes;

import java.util.stream.Stream;

import eu4.comsim.core.Technology;
import eu4.comsim.gui.TechnologyGroup;

/**
 * Enumeration of all Units unlockable in the game.<br>
 *
 * @see <a href="http://www.eu4wiki.com/Land_units">http://www.eu4wiki.com/
 *      Land_units</a>
 */
public enum Unit {

    ADAL_GUERILLA_WARFARE(23, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Adal Guerrilla Warfare", 3, 3, 2, 2, 2, 3),
    ADAL_GUNPOWDER_WARFARE(15, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Adal Gunpowder Warfare", 2, 2, 2, 2, 2, 3),
    AFRICAN_ABYSSINIAN_CAVALRY(10, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Abyssinian Barded Cavalry", 0, 0, 3, 2, 3, 2),
    AFRICAN_ABYSSINIAN_LIGHT_CAVALRY(1, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Abyssinian Barded Cavalry", 0, 0, 1, 1, 2, 0),
    AFRICAN_CLUBMEN(1, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Clubmen", 0, 0, 1, 0, 1, 1),
    AFRICAN_CUIRASSIER(28, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "African Dragoon", 1, 1, 4, 4, 4, 4),
    AFRICAN_DRAGOON(23, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "African Dragoon", 1, 1, 3, 3, 4, 4),
    AFRICAN_HILL_WARFARE(12, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Hill Warfare", 2, 1, 1, 2, 2, 2),
    AFRICAN_HUSSAR(14, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "African Hussar", 1, 0, 3, 2, 3, 3),
    AFRICAN_MANDELAKU(1, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Mandelaku Cavalry", 0, 0, 1, 1, 1, 1),
    AFRICAN_MOSSI_HORSEMEN(10, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Mossi Horsemen", 0, 0, 2, 2, 3, 3),
    AFRICAN_SOMALI_CAVALRY(6, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Somali Light Cavalry", 0, 0, 2, 1, 1, 2),
    AFRICAN_SPEARMEN(1, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Spearmen Tactics", 0, 0, 0, 1, 1, 1),
    AFRICAN_SWARM(17, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "African Swarm Cavalry", 0, 1, 4, 3, 3, 2),
    AFRICAN_TUAREG_CAVALRY(6, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Tuareg Cavalry", 0, 0, 2, 1, 2, 1),
    AFRICAN_WESTERN_FRANCHISE_WARFARE(30, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Western Franchise Warfare", 3, 3, 3, 3, 4, 4),
    AFSHARID_REFORMED(18, Technology.Group.MUSLIM, UnitType.CAVALRY, "Afsharid Reformed Tactics", 0, 1, 4, 3, 3, 3),
    AFSHARID_REFORMED_INFANTRY(15, Technology.Group.MUSLIM, UnitType.INFANTRY, "Afsharid Reformed Infantry", 3, 2, 2, 2, 2, 2),
    ALGONKIN_TOMAHAWK_CHARGE(5, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Algonkin Tomahawk Charge Tactics", 0, 0, 2, 1, 2, 1),
    ANGLOFRENCH_LINE(23, Technology.Group.WESTERN, UnitType.INFANTRY, "Anglo-French Line", 3, 3, 2, 2, 3, 3),
    APACHE_GUERILLA(26, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Apache Guerrilla Tactics", 4, 3, 2, 4, 3, 3),
    ASIAN_ARQUEBUSIER(9, Technology.Group.CHINESE, UnitType.INFANTRY, "Asian Arquebusier", 1, 1, 1, 1, 1, 1),
    ASIAN_CHARGE_CAVALRY(14, Technology.Group.CHINESE, UnitType.CAVALRY, "Asian Charge Cavalry", 0, 1, 3, 2, 3, 3),
    ASIAN_MASS_INFANTRY(15, Technology.Group.CHINESE, UnitType.INFANTRY, "Asian Mass Infantry", 3, 2, 2, 2, 2, 2),
    ASIAN_MUSKETEER(19, Technology.Group.CHINESE, UnitType.INFANTRY, "Asian Musketeer", 3, 3, 2, 2, 3, 2),
    AUSTRIAN_GRENZER(23, Technology.Group.WESTERN, UnitType.INFANTRY, "Austrian Grenzer", 2, 3, 2, 3, 3, 3),
    AUSTRIAN_HUSSAR(23, Technology.Group.WESTERN, UnitType.CAVALRY, "Austrian Hussar", 1, 2, 4, 3, 4, 4),
    AUSTRIAN_JAEGER(30, Technology.Group.WESTERN, UnitType.INFANTRY, "Austrian Jaeger", 4, 4, 4, 3, 3, 4),
    AUSTRIAN_TERCIO(19, Technology.Group.WESTERN, UnitType.INFANTRY, "Austrian Tercio", 2, 2, 2, 3, 3, 3),
    AUSTRIAN_WHITE_COAT(26, Technology.Group.WESTERN, UnitType.INFANTRY, "Austrian White Coat", 4, 3, 3, 3, 3, 4),
    AZTEC_GUNPOWDER_WARFARE(14, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Aztec Gunpowder Warfare", 2, 1, 3, 2, 2, 2),
    AZTEC_HILL_WARFARE(10, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Aztec Hill Warfare", 0, 0, 3, 1, 2, 2),
    AZTEC_TRIBAL_WARFARE(5, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Aztec Tribal Warfare", 0, 0, 2, 1, 2, 1),
    BANTU_GUNPOWDER_WARFARE(15, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Bantu Gunpowder Warfare", 3, 2, 3, 2, 2, 1),
    BANTU_PLAINS_WARFARE(12, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Bantu Plains Warfare", 2, 1, 2, 2, 2, 1),
    BANTU_TRIBAL_WARFARE(5, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Bantu Tribal Warfare", 0, 0, 2, 1, 2, 1),
    BARDICHE_INFANTRY(1, Technology.Group.EASTERN, UnitType.INFANTRY, "Bardiche Infantry", 0, 0, 0, 0, 1, 1),
    BHONSLE_CAVALRY(28, Technology.Group.INDIAN, UnitType.CAVALRY, "Bhonsle Cavalry tactics", 1, 2, 4, 4, 4, 3),
    BHONSLE_INFANTRY(18, Technology.Group.INDIAN, UnitType.INFANTRY, "Bhonsle Infantry Tactics", 3, 3, 2, 2, 2, 2),
    BRITISH_HUSSAR(26, Technology.Group.WESTERN, UnitType.CAVALRY, "British Hussar", 1, 2, 4, 4, 4, 4),
    BRITISH_REDCOAT(26, Technology.Group.WESTERN, UnitType.INFANTRY, "British Red Coat", 3, 4, 3, 3, 4, 3),
    BRITISH_SQUARE(28, Technology.Group.WESTERN, UnitType.INFANTRY, "British Square", 3, 4, 3, 3, 4, 4),
    CENTRAL_AMERICAN_DRAGOON(23, Technology.Group.MESOAMERICAN, UnitType.CAVALRY, "American Dragoon", 2, 2, 3, 3, 4, 4),
    CENTRAL_AMERICAN_HORSEMEN(6, Technology.Group.MESOAMERICAN, UnitType.CAVALRY, "American Horsemen", 0, 0, 2, 1, 1, 1),
    CENTRAL_AMERICAN_HUSSAR(14, Technology.Group.MESOAMERICAN, UnitType.CAVALRY, "American Hussar", 1, 1, 3, 2, 3, 2),
    CENTRAL_AMERICAN_RIFLE_CAVALRY(10, Technology.Group.MESOAMERICAN, UnitType.CAVALRY, "American Rifle Cavalry", 0, 1, 3, 2, 2, 2),
    CENTRAL_AMERICAN_SWARM(19, Technology.Group.MESOAMERICAN, UnitType.CAVALRY, "American Swarm Cavalry", 1, 2, 3, 3, 3, 2),
    CHAMBERED_DEMI_CANNON(16, Technology.Group.WESTERN, UnitType.ARTILLERY, "Chambered Demi Cannon", 2, 2, 0, 1, 2, 1),
    CHEVAUCHEE(1, Technology.Group.WESTERN, UnitType.CAVALRY, "Chevauch�e", 0, 0, 1, 1, 1, 0),
    CHINESE_DRAGOON(23, Technology.Group.CHINESE, UnitType.CAVALRY, "Chinese Dragoon", 1, 2, 3, 3, 4, 3),
    CHINESE_FOOTSOLDIER(5, Technology.Group.CHINESE, UnitType.INFANTRY, "Chinese Footsoldier", 0, 0, 1, 0, 2, 1),
    CHINESE_LONGSPEAR(1, Technology.Group.CHINESE, UnitType.INFANTRY, "Chinese Longspear", 0, 0, 0, 1, 0, 1),
    CHINESE_STEPPE(6, Technology.Group.CHINESE, UnitType.CAVALRY, "Chinese Steppe", 0, 0, 2, 2, 1, 1),
    COEHORN_MORTAR(22, Technology.Group.WESTERN, UnitType.ARTILLERY, "Coehorn Mortar", 3, 3, 1, 1, 3, 3),
    COMMANCHE_SWARM(19, Technology.Group.NORTH_AMERICAN, UnitType.CAVALRY, "American Swarm Cavalry", 1, 2, 3, 3, 3, 2),
    CREEK_ARQUEBUSIER(14, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Creek Arquebusier", 2, 2, 2, 2, 2, 2),
    CULVERIN(10, Technology.Group.WESTERN, UnitType.ARTILLERY, "Culverin", 1, 1, 0, 1, 1, 0),
    DRUZHINA_CAVALRY(1, Technology.Group.EASTERN, UnitType.CAVALRY, "Druzhina Cavalry", 0, 0, 0, 1, 1, 1),
    DURRANI_DRAGOON(28, Technology.Group.MUSLIM, UnitType.CAVALRY, "Durrani Dragoon", 2, 1, 5, 3, 4, 4),
    DURRANI_RIFLED_MUSKETEER(23, Technology.Group.MUSLIM, UnitType.INFANTRY, "Durrani Rifled Musketeer", 1, 2, 3, 3, 4, 3),
    DURRANI_SWIVEL(28, Technology.Group.MUSLIM, UnitType.CAVALRY, "Durrani Swivel tactics", 1, 2, 4, 5, 4, 3),
    DUTCH_MAURICIAN(15, Technology.Group.WESTERN, UnitType.INFANTRY, "Dutch Maurician", 2, 2, 2, 1, 3, 2),
    EAST_ASIAN_SPEARMEN(1, Technology.Group.CHINESE, UnitType.INFANTRY, "East Asian Spearmen", 0, 0, 0, 1, 1, 0),
    EAST_MONGOLIAN_STEPPE(10, Technology.Group.CHINESE, UnitType.CAVALRY, "East Mongolian Steppe Tactics", 1, 0, 2, 2, 2, 2),
    EASTERN_BOW(1, Technology.Group.CHINESE, UnitType.CAVALRY, "Eastern Bow Tactics", 0, 0, 1, 0, 1, 1),
    EASTERN_CARABINIER(26, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Eastern Carabinier", 4, 3, 3, 3, 3, 3),
    EASTERN_KNIGHTS(1, Technology.Group.EASTERN, UnitType.CAVALRY, "Eastern Knights", 0, 0, 1, 0, 1, 1),
    EASTERN_MILITIA(5, Technology.Group.EASTERN, UnitType.INFANTRY, "Eastern Militia", 0, 0, 1, 1, 1, 1),
    EASTERN_SKIRMISHER(26, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Eastern Skirmisher", 1, 2, 4, 3, 4, 3),
    EASTERN_UHLAN(26, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Eastern Uhlan", 2, 1, 3, 4, 4, 3),
    ETHIOPIAN_GUERILLA_WARFARE(23, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Ethiopian Guerrilla Warfare", 2, 3, 2, 3, 2, 3),
    ETHIOPIAN_GUNPOWDER_WARFARE(15, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Ethiopian Gunpowder Warfare", 2, 3, 2, 3, 1, 2),
    ETHIOPIAN_MOUNTAIN_WARFARE(12, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Ethiopian Mountain Warfare", 1, 2, 2, 2, 1, 2),
    FLYING_BATTERY(29, Technology.Group.WESTERN, UnitType.ARTILLERY, "Flying Battery", 4, 4, 2, 2, 4, 4),
    FRENCH_BLUECOAT(26, Technology.Group.WESTERN, UnitType.INFANTRY, "French Blue Coat", 3, 3, 3, 3, 4, 4),
    FRENCH_CARABINIER(26, Technology.Group.WESTERN, UnitType.CAVALRY, "French Carabinier", 2, 1, 5, 4, 3, 4),
    FRENCH_CARACOLLE(14, Technology.Group.WESTERN, UnitType.CAVALRY, "French Caracole", 1, 0, 3, 2, 2, 2),
    FRENCH_CUIRASSIER(28, Technology.Group.WESTERN, UnitType.CAVALRY, "French Cuirassier", 1, 2, 5, 4, 4, 5),
    FRENCH_DRAGOON(23, Technology.Group.WESTERN, UnitType.CAVALRY, "French Dragoon", 1, 1, 4, 4, 4, 4),
    FRENCH_IMPULSE(28, Technology.Group.WESTERN, UnitType.INFANTRY, "French Impulse", 4, 3, 3, 3, 4, 4),
    GAELIC_FREE_SHOOTER(12, Technology.Group.WESTERN, UnitType.INFANTRY, "Gaelic Free Shooter", 1, 2, 2, 1, 3, 1),
    GAELIC_GALLOGLAIGH(5, Technology.Group.WESTERN, UnitType.INFANTRY, "Gaelic Galloglaigh", 0, 0, 1, 0, 2, 0),
    GAELIC_MERCENARY(9, Technology.Group.WESTERN, UnitType.INFANTRY, "Gaelic Mercenary", 0, 0, 2, 0, 2, 1),
    GERMANIZED_PIKE(9, Technology.Group.EASTERN, UnitType.INFANTRY, "Germanized Pike", 0, 0, 1, 2, 1, 2),
    HALBERD_INFANTRY(1, Technology.Group.WESTERN, UnitType.INFANTRY, "Halberd Infantry", 0, 0, 1, 0, 1, 0),
    HAN_BANNER(12, Technology.Group.CHINESE, UnitType.INFANTRY, "Han Banner Army", 1, 2, 2, 1, 2, 2),
    HOUFNICE(7, Technology.Group.WESTERN, UnitType.ARTILLERY, "Houfnice", 1, 0, 0, 0, 0, 1),
    HUNGARIAN_HUSSAR(10, Technology.Group.EASTERN, UnitType.CAVALRY, "Hungarian Hussar", 0, 0, 2, 2, 3, 2),
    HURON_ARQUEBUSIER(14, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Huron Arquebusier Tactics", 2, 2, 2, 1, 3, 2),
    INCA_MOUNTAIN_WARFARE(5, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Inca Mountain Warfare", 0, 0, 1, 2, 1, 2),
    INCAN_AXEMEN(5, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Incan Axmen", 0, 0, 1, 1, 2, 2),
    INCAN_GUERILLA_WARFARE(19, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Incan Guerilla Warfare", 2, 3, 2, 2, 2, 3),
    INCAN_SLINGSHOTS(5, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Incan Slingshots", 0, 0, 2, 2, 1, 1),
    INDIAN_ARCHERS(1, Technology.Group.INDIAN, UnitType.CAVALRY, "Indian Cavalry Archers", 0, 0, 1, 0, 1, 1),
    INDIAN_ARQUEBUSIER(5, Technology.Group.INDIAN, UnitType.INFANTRY, "Indian Arquebusier", 1, 0, 1, 1, 1, 0),
    INDIAN_ELEPHANT(10, Technology.Group.INDIAN, UnitType.CAVALRY, "Indian Elephant Tactics", 1, 0, 3, 2, 2, 2),
    INDIAN_FOOTSOLDIER(1, Technology.Group.INDIAN, UnitType.INFANTRY, "Indian Footsoldier", 0, 0, 1, 0, 1, 1),
    INDIAN_RIFLE(26, Technology.Group.INDIAN, UnitType.INFANTRY, "Indian Rifle Tactics", 3, 3, 3, 3, 3, 4),
    INDIAN_SHOCK_CAVALRY(23, Technology.Group.INDIAN, UnitType.CAVALRY, "Indian Shock Cavalry", 1, 1, 4, 3, 4, 3),
    IRISH_CHARGE(15, Technology.Group.WESTERN, UnitType.INFANTRY, "Irish Charge", 1, 2, 3, 1, 3, 2),
    IROQUOIS_RIFLE_SCOUT(19, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Iroquois Rifle Scout Tactics", 3, 3, 2, 2, 2, 2),
    ITALIAN_CONDOTTA(9, Technology.Group.WESTERN, UnitType.INFANTRY, "Italian Condotta", 0, 0, 2, 1, 1, 1),
    JAPANESE_ARCHER(1, Technology.Group.CHINESE, UnitType.INFANTRY, "Japanese Archer", 0, 0, 1, 0, 0, 1),
    JAPANESE_FOOTSOLDIER(5, Technology.Group.CHINESE, UnitType.INFANTRY, "Japanese Footsoldier", 0, 0, 1, 1, 1, 1),
    JAPANESE_SAMURAI(6, Technology.Group.CHINESE, UnitType.CAVALRY, "Japanese Samurai", 0, 0, 2, 1, 2, 1),
    LARGE_CAST_BRONZE_MORTAR(7, Technology.Group.WESTERN, UnitType.ARTILLERY, "Large Cast Bronze Mortar", 1, 0, 0, 0, 1, 0),
    LARGE_CAST_IRON_BOMBARD(13, Technology.Group.WESTERN, UnitType.ARTILLERY, "Large Cast Iron Bombard", 2, 1, 0, 0, 1, 2),
    LEATHER_CANNON(18, Technology.Group.WESTERN, UnitType.ARTILLERY, "Leather Cannon", 2, 2, 1, 1, 2, 2),
    MAHARATHAN_CAVALRY(17, Technology.Group.INDIAN, UnitType.CAVALRY, "Maharathan Cavalry", 0, 2, 4, 3, 3, 2),
    MAHARATHAN_GUERILLA_WARFARE(23, Technology.Group.INDIAN, UnitType.INFANTRY, "Maharathan Guerilla Warfare", 4, 3, 2, 2, 2, 3),
    MALI_TRIBAL_WARFARE(5, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Mali Tribal Warfare", 0, 0, 2, 2, 1, 1),
    MAMLUK_ARCHER(1, Technology.Group.MUSLIM, UnitType.INFANTRY, "Mamluk Archer", 0, 0, 1, 1, 0, 1),
    MAMLUK_CAVALRY_CHARGE(1, Technology.Group.MUSLIM, UnitType.CAVALRY, "Mamluk Cavalry Charge", 0, 0, 2, 0, 1, 1),
    MAMLUK_DUEL(5, Technology.Group.MUSLIM, UnitType.INFANTRY, "Mamluk Duel", 0, 0, 1, 1, 1, 1),
    MAMLUK_MUSKET_CHARGE(18, Technology.Group.MUSLIM, UnitType.CAVALRY, "Mamluk Musket Charge", 1, 1, 4, 3, 3, 2),
    MANCHU_BANNER(17, Technology.Group.CHINESE, UnitType.CAVALRY, "Manchu Banner Army", 1, 1, 3, 3, 3, 3),
    MAYA_FOREST_WARFARE(10, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Mayan Forest Warfare", 0, 0, 2, 2, 2, 2),
    MAYA_GUERILLA_WARFARE(19, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Mayan Guerrilla Warfare", 3, 2, 3, 2, 2, 2),
    MAYA_GUNPOWDER_WARFARE(14, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Mayan Gunpowder Warfare", 2, 2, 2, 2, 2, 2),
    MAYA_TRIBAL_WARFARE(5, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Mayan Tribal Warfare", 0, 0, 2, 1, 1, 2),
    MESOAMERICAN_SPEARMEN(1, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Mesoamerican Spearmen", 0, 0, 1, 0, 1, 1),
    MEXICAN_GUERILLA_WARFARE(19, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Mexican Guerrilla Warfare", 2, 2, 2, 2, 3, 3),
    MIXED_ORDER_INFANTRY(30, Technology.Group.WESTERN, UnitType.INFANTRY, "Mixed Order Infantry", 4, 3, 4, 3, 4, 4),
    MONGOL_BOW(1, Technology.Group.NOMAD_GROUP, UnitType.INFANTRY, "Mongol Bow", 0, 0, 1, 1, 1, 1),
    MONGOL_STEPPE(1, Technology.Group.NOMAD_GROUP, UnitType.CAVALRY, "Mongol Steppe", 0, 0, 1, 1, 2, 2),
    MONGOL_SWARM(1, Technology.Group.NOMAD_GROUP, UnitType.CAVALRY, "Mongol Swarm", 0, 0, 2, 1, 2, 1),
    MONGOLIAN_BOW(1, Technology.Group.CHINESE, UnitType.CAVALRY, "Mongolian Bow Tactics", 0, 0, 0, 1, 1, 1),
    MUGHAL_MANSABDAR(6, Technology.Group.INDIAN, UnitType.CAVALRY, "Mughal Mansabdar", 0, 0, 2, 2, 1, 1),
    MUGHAL_MUSKETEER(9, Technology.Group.INDIAN, UnitType.INFANTRY, "Mughal Musketeer", 1, 1, 1, 1, 2, 1),
    MUSCOVITE_CARACOLLE(14, Technology.Group.EASTERN, UnitType.CAVALRY, "Muscovite Caracolle", 2, 0, 2, 2, 3, 3),
    MUSCOVITE_COSSACK(22, Technology.Group.EASTERN, UnitType.CAVALRY, "Muscovite Cossack", 1, 1, 4, 3, 4, 4),
    MUSCOVITE_MUSKETEER(12, Technology.Group.EASTERN, UnitType.INFANTRY, "Muscovite Musketeer", 2, 1, 2, 1, 3, 2),
    MUSCOVITE_SOLDATY(15, Technology.Group.EASTERN, UnitType.INFANTRY, "Muscovite Soldaty", 3, 1, 2, 1, 3, 2),
    MUSLIM_CAVALRY_ARCHERS(1, Technology.Group.MUSLIM, UnitType.CAVALRY, "Muslim Cavalry Archers", 0, 0, 1, 1, 1, 1),
    MUSLIM_DRAGOON(23, Technology.Group.MUSLIM, UnitType.CAVALRY, "Muslim Dragoon", 1, 2, 3, 4, 3, 4),
    MUSLIM_MASS_INFANTRY(26, Technology.Group.MUSLIM, UnitType.INFANTRY, "Muslim Mass Infantry", 3, 4, 3, 3, 3, 3),
    NAPOLEONIC_LANCERS(28, Technology.Group.WESTERN, UnitType.CAVALRY, "Napoleonic Lancers", 0, 2, 6, 4, 5, 4),
    NAPOLEONIC_SQUARE(30, Technology.Group.WESTERN, UnitType.INFANTRY, "Napoleonic Square", 4, 4, 4, 3, 4, 3),
    NATIVE_CLUBMEN(1, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Native Clubmen", 0, 0, 1, 0, 1, 1),
    NATIVE_INDIAN_ARCHER(1, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Native Indian Archer", 0, 0, 0, 1, 1, 1),
    NATIVE_INDIAN_HORSEMEN(6, Technology.Group.NORTH_AMERICAN, UnitType.CAVALRY, "American Horsemen", 0, 0, 2, 1, 1, 1),
    NATIVE_INDIAN_MOUNTAIN_WARFARE(10, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Native Indian Mountain Warfare", 1, 1, 1, 2, 2, 1),
    NATIVE_INDIAN_TRIBAL_WARFARE(5, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Native Indian Tribal Warfare", 0, 0, 2, 1, 1, 2),
    NIGER_KONGOLESE_FOREST_WARFARE(12, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Niger-Kongolese Forest Warfare", 1, 1, 2, 2, 2, 2),
    NIGER_KONGOLESE_GUERILLA_WARFARE(23, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Niger Kongolese Guerrilla Warfare", 3, 2, 2, 3, 3, 2),
    NIGER_KONGOLESE_GUNPOWDER_WARFARE(15, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Niger-Kongolese Gunpowder Warfare", 3, 2, 2, 2, 2, 2),
    NIGER_KONGOLESE_TRIBAL_WARFARE(5, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Niger Kongolese Tribal Warfare", 0, 0, 1, 1, 2, 2),
    NORTH_AMERICAN_HUSSAR(14, Technology.Group.NORTH_AMERICAN, UnitType.CAVALRY, "American Hussar", 1, 1, 3, 2, 3, 2),
    NORTH_AMERICAN_RIFLE_CAVALRY(10, Technology.Group.NORTH_AMERICAN, UnitType.CAVALRY, "American Rifle Cavalry", 0, 1, 3, 2, 2, 2),
    OPEN_ORDER_CAVALRY(28, Technology.Group.WESTERN, UnitType.CAVALRY, "Open Order Cavalry", 2, 1, 4, 4, 5, 5),
    OTTOMAN_AZAB(5, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Ottoman Azab", 0, 0, 1, 1, 2, 1),
    OTTOMAN_JANISSARY(9, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Ottoman Janissary Tactics", 1, 0, 1, 1, 2, 2),
    OTTOMAN_LANCER(28, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Ottoman Lancer", 1, 1, 5, 3, 5, 3),
    OTTOMAN_MUSELLEM(1, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Ottoman M�sellem", 0, 0, 2, 1, 1, 1),
    OTTOMAN_NEW_MODEL(30, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Ottoman New Model", 3, 3, 3, 3, 4, 4),
    OTTOMAN_NIZAMI_CEDID(23, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Ottoman Nizam-i Cedid", 2, 3, 2, 2, 3, 3),
    OTTOMAN_REFORMED_JANISSARY(19, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Ottoman Reformed Janissary", 2, 2, 2, 2, 3, 2),
    OTTOMAN_REFORMED_SPAHI(18, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Ottoman Reformed Spahi", 1, 1, 3, 3, 3, 3),
    OTTOMAN_SEKBAN(12, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Ottoman Sekban", 2, 2, 2, 1, 2, 3),
    OTTOMAN_SPAHI(10, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Ottoman Spahi", 0, 0, 3, 2, 3, 2),
    OTTOMAN_TIMARIOT(6, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Ottoman Timariot", 0, 0, 2, 1, 2, 1),
    OTTOMAN_TOPRAKLI_DRAGOON(28, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Ottoman Toprakli Dragoon", 2, 2, 3, 3, 4, 4),
    OTTOMAN_TOPRAKLI_HIT_AND_RUN(23, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Ottoman Toprakli Hit-and-run", 2, 0, 4, 3, 4, 3),
    OTTOMAN_YAYA(1, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Ottoman Yaya", 0, 0, 1, 0, 1, 1),
    PEDRERO(10, Technology.Group.WESTERN, UnitType.ARTILLERY, "Pedrero", 1, 1, 0, 0, 1, 1),
    PERSIAN_CAVALRY_CHARGE(1, Technology.Group.MUSLIM, UnitType.CAVALRY, "Persian Cavalry Charge", 0, 0, 1, 0, 2, 1),
    PERSIAN_FOOTSOLDIER(1, Technology.Group.MUSLIM, UnitType.INFANTRY, "Persian Footsoldier", 0, 0, 1, 0, 1, 1),
    PERSIAN_RIFLE(30, Technology.Group.MUSLIM, UnitType.INFANTRY, "Persian Rifle", 3, 3, 3, 3, 4, 4),
    PERSIAN_SHAMSHIR(9, Technology.Group.MUSLIM, UnitType.INFANTRY, "Persian Shamshir", 0, 0, 2, 1, 2, 2),
    PERUVIAN_GUERILLA_WARFARE(19, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Peruvian Guerrilla Warfare", 2, 2, 2, 2, 3, 3),
    POLISH_HUSSAR(14, Technology.Group.EASTERN, UnitType.CAVALRY, "Polish Hussar", 1, 1, 3, 3, 2, 2),
    POLISH_MUSKETEER(12, Technology.Group.EASTERN, UnitType.INFANTRY, "Polish Musketeer", 2, 2, 1, 2, 2, 2),
    POLISH_TERCIO(15, Technology.Group.EASTERN, UnitType.INFANTRY, "Polish Tercio", 2, 2, 1, 2, 2, 3),
    POLISH_WINGED_HUSSAR(22, Technology.Group.EASTERN, UnitType.CAVALRY, "Polish Winged Hussar", 0, 1, 5, 4, 4, 3),
    PRUSSIAN_DRILL(30, Technology.Group.WESTERN, UnitType.INFANTRY, "Prussian Drill", 4, 4, 3, 3, 4, 4),
    PRUSSIAN_FREDERICKIAN(26, Technology.Group.WESTERN, UnitType.INFANTRY, "Prussian Frederickian", 4, 3, 3, 3, 4, 3),
    PRUSSIAN_UHLAN(26, Technology.Group.WESTERN, UnitType.CAVALRY, "Prussian Uhlan", 1, 2, 5, 4, 4, 3),
    PUEBLO_AMBUSH(5, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Pueblo Ambush Tactics", 0, 0, 1, 2, 2, 1),
    QIZILBASH_CAVALRY(10, Technology.Group.MUSLIM, UnitType.CAVALRY, "Qizilbash Cavalry", 0, 0, 2, 2, 3, 2),
    RAJPUT_HILL_FIGHTERS(1, Technology.Group.INDIAN, UnitType.CAVALRY, "Rajput Hill Fighters", 0, 0, 0, 1, 1, 1),
    RAJPUT_MUSKETEER(12, Technology.Group.INDIAN, UnitType.INFANTRY, "Rajput Musketeer", 2, 2, 1, 2, 2, 2),
    REFORMED_ASIAN_CAVALRY(28, Technology.Group.CHINESE, UnitType.CAVALRY, "Reformed Asian Cavalry Tactics", 1, 2, 5, 4, 3, 4),
    REFORMED_ASIAN_MUSKETEER(26, Technology.Group.CHINESE, UnitType.INFANTRY, "Reformed Asian Musketeer", 4, 4, 3, 3, 3, 3),
    REFORMED_MANCHU_RIFLE(28, Technology.Group.CHINESE, UnitType.CAVALRY, "Reformed Manchu Rifle Tactics", 2, 1, 4, 4, 4, 4),
    REFORMED_MUGHAL_MANSABDAR(14, Technology.Group.INDIAN, UnitType.CAVALRY, "Reformed Mughal Mansabdar", 2, 0, 2, 3, 3, 2),
    REFORMED_MUGHAL_MUSKETEER(12, Technology.Group.INDIAN, UnitType.INFANTRY, "Reformed Mughal Musketeer", 2, 1, 2, 1, 3, 2),
    REFORMED_STEPPE_RIFLES(30, Technology.Group.NOMAD_GROUP, UnitType.INFANTRY, "Horde TL 30 Infantry", 3, 3, 3, 3, 4, 4),
    REFORMED_WESTERNIZED_INCAN(30, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Reformed Westernized Incan", 4, 4, 3, 3, 3, 3),
    ROYAL_MORTAR(25, Technology.Group.WESTERN, UnitType.ARTILLERY, "Royal Mortar", 4, 4, 1, 1, 3, 3),
    RUSSIAN_COSSACK(28, Technology.Group.EASTERN, UnitType.CAVALRY, "Russian Cossack", 2, 1, 4, 4, 5, 4),
    RUSSIAN_CUIRASSIER(28, Technology.Group.EASTERN, UnitType.CAVALRY, "Russian Cuirassier", 1, 2, 5, 4, 4, 4),
    RUSSIAN_GREEN_COAT(26, Technology.Group.EASTERN, UnitType.INFANTRY, "Russian Green Coat", 3, 3, 3, 3, 4, 4),
    RUSSIAN_LANCER(26, Technology.Group.EASTERN, UnitType.CAVALRY, "Russian Lancer", 0, 1, 5, 4, 4, 4),
    RUSSIAN_MASS(30, Technology.Group.EASTERN, UnitType.INFANTRY, "Russian Mass", 4, 3, 4, 3, 4, 3),
    RUSSIAN_PETRINE(23, Technology.Group.EASTERN, UnitType.INFANTRY, "Russian Petrine", 2, 3, 2, 3, 3, 3),
    SAXON_INFANTRY(19, Technology.Group.EASTERN, UnitType.INFANTRY, "Saxon Infantry", 3, 2, 3, 2, 2, 2),
    SCHWARZE_REITER(10, Technology.Group.WESTERN, UnitType.CAVALRY, "Schwarze Reiter", 1, 0, 2, 1, 2, 2),
    SCOTTISH_HIGHLANDER(19, Technology.Group.WESTERN, UnitType.INFANTRY, "Scottish Highlander", 2, 2, 3, 2, 4, 2),
    SHAYBANI(6, Technology.Group.MUSLIM, UnitType.CAVALRY, "Shaybanid Tactics", 0, 0, 2, 1, 1, 2),
    SIKH_HIT_AND_RUN(18, Technology.Group.INDIAN, UnitType.INFANTRY, "Sikh - Hit and Run", 3, 2, 2, 2, 3, 2),
    SIKH_RIFLE(28, Technology.Group.INDIAN, UnitType.CAVALRY, "Sikh Rifle Tactics", 2, 1, 4, 4, 3, 4),
    SIOUX_DRAGOON(23, Technology.Group.NORTH_AMERICAN, UnitType.CAVALRY, "American Dragoon", 2, 2, 3, 3, 4, 4),
    SLAVIC_STRADIOTI(6, Technology.Group.EASTERN, UnitType.CAVALRY, "Slavic Stradioti", 0, 0, 2, 1, 1, 1),
    SMALL_CAST_IRON_BOMBARD(13, Technology.Group.WESTERN, UnitType.ARTILLERY, "Small Cast Iron Bombard", 1, 1, 0, 1, 2, 1),
    SONGHAI_TRIBAL_WARFARE(5, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Songhai Tribal Warfare", 0, 0, 1, 2, 1, 2),
    SOUTH_AMERICAN_ARQUEBUSIER(10, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "South American Arquebusier", 1, 2, 1, 1, 2, 1),
    SOUTH_AMERICAN_DRAGOON(23, Technology.Group.SOUTH_AMERICAN, UnitType.CAVALRY, "American Dragoon", 2, 2, 3, 3, 4, 4),
    SOUTH_AMERICAN_FOREST_WARFARE(5, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "South American Forest Warfare", 0, 0, 1, 2, 2, 1),
    SOUTH_AMERICAN_GUNPOWDER_WARFARE(10, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "South American Gunpowder Warfare", 1, 1, 1, 2, 1, 2),
    SOUTH_AMERICAN_HORSEMEN(6, Technology.Group.SOUTH_AMERICAN, UnitType.CAVALRY, "American Horsemen", 0, 0, 2, 1, 1, 1),
    SOUTH_AMERICAN_HUSSAR(14, Technology.Group.SOUTH_AMERICAN, UnitType.CAVALRY, "American Hussar", 1, 1, 3, 2, 3, 2),
    SOUTH_AMERICAN_REFORMED_GUNPOWDER_WARFARE(14, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "South American Reformed Gunpowder Warfare", 2, 2, 2, 2, 2, 2),
    SOUTH_AMERICAN_RIFLE_CAVALRY(10, Technology.Group.SOUTH_AMERICAN, UnitType.CAVALRY, "American Rifle Cavalry", 0, 1, 3, 2, 2, 2),
    SOUTH_AMERICAN_SPEARMEN(1, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "South American Spearmen", 0, 0, 1, 1, 1, 0),
    SOUTH_AMERICAN_SWARM(19, Technology.Group.SOUTH_AMERICAN, UnitType.CAVALRY, "American Swarm Cavalry", 1, 2, 3, 3, 3, 2),
    SOUTH_AMERICAN_WARFARE(1, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "South American Warfare", 0, 0, 0, 1, 1, 1),
    SOUTH_INDIAN_MUSKETEER(9, Technology.Group.INDIAN, UnitType.INFANTRY, "South Indian Musketeer", 1, 2, 1, 1, 1, 1),
    SPANISH_TERCIO(12, Technology.Group.WESTERN, UnitType.INFANTRY, "Spanish Tercio", 1, 2, 1, 2, 2, 2),
    STEPPE_CAVALRY(23, Technology.Group.NOMAD_GROUP, UnitType.CAVALRY, "Horde TL 23 Cavalry", 1, 1, 4, 3, 3, 3),
    STEPPE_FOOTMEN(12, Technology.Group.NOMAD_GROUP, UnitType.INFANTRY, "Horde TL 12 Infantry", 1, 1, 2, 2, 2, 2),
    STEPPE_INFANTRY(19, Technology.Group.NOMAD_GROUP, UnitType.INFANTRY, "Horde TL 19 Infantry", 2, 2, 3, 3, 3, 2),
    STEPPE_LANCERS(14, Technology.Group.NOMAD_GROUP, UnitType.CAVALRY, "Horde TL 14 Cavalry", 0, 0, 3, 3, 3, 3),
    STEPPE_MOUNTED_RAIDERS(18, Technology.Group.NOMAD_GROUP, UnitType.CAVALRY, "Horde TL 18 Cavalry", 0, 1, 3, 3, 3, 3),
    STEPPE_MUSKETEERS(15, Technology.Group.NOMAD_GROUP, UnitType.INFANTRY, "Horde TL 15 Infantry", 2, 1, 3, 2, 2, 2),
    STEPPE_RAIDERS(9, Technology.Group.NOMAD_GROUP, UnitType.INFANTRY, "Horde TL 9 Infantry", 0, 0, 2, 1, 1, 2),
    STEPPE_RIDERS(10, Technology.Group.NOMAD_GROUP, UnitType.CAVALRY, "Horde TL 10 Cavalry", 0, 0, 3, 2, 3, 2),
    STEPPE_RIFLES(26, Technology.Group.NOMAD_GROUP, UnitType.INFANTRY, "Horde TL 26 Infantry", 2, 3, 3, 3, 4, 4),
    STEPPE_UHLANS(28, Technology.Group.NOMAD_GROUP, UnitType.CAVALRY, "Horde TL 28 Cavalry", 1, 1, 5, 4, 4, 3),
    SWEDISH_ARME_BLANCHE(23, Technology.Group.WESTERN, UnitType.CAVALRY, "Swedish Arme Blanche", 1, 1, 5, 3, 5, 3),
    SWEDISH_CAROLINE(23, Technology.Group.WESTERN, UnitType.INFANTRY, "Swedish Caroline", 3, 2, 3, 2, 3, 3),
    SWEDISH_GALLOP(18, Technology.Group.WESTERN, UnitType.CAVALRY, "Swedish Gallop", 1, 1, 4, 3, 3, 3),
    SWEDISH_GUSTAVIAN(19, Technology.Group.WESTERN, UnitType.INFANTRY, "Swedish Gustavian", 3, 2, 3, 2, 3, 2),
    SWISS_LANDSKNECHTEN(9, Technology.Group.WESTERN, UnitType.INFANTRY, "Swiss Landsknechten", 0, 0, 1, 1, 1, 2),
    SWIVEL_CANNON(20, Technology.Group.WESTERN, UnitType.ARTILLERY, "Swivel Cannon", 3, 2, 1, 1, 3, 2),
    TARTAR_COSSACK(26, Technology.Group.EASTERN, UnitType.CAVALRY, "Tartar Cossack", 1, 1, 4, 4, 4, 4),
    TIPU_SULTAN_ROCKET(30, Technology.Group.INDIAN, UnitType.INFANTRY, "Tipu Sultan Rocket Tactics", 4, 3, 3, 3, 4, 3),
    TOFONGCHIS_MUSKETEER(12, Technology.Group.MUSLIM, UnitType.INFANTRY, "Tofongchis Musketeers", 2, 1, 1, 2, 2, 3),
    TOPCHIS_ARTILLERY(14, Technology.Group.MUSLIM, UnitType.CAVALRY, "Topchis Cavalry", 1, 0, 2, 2, 3, 2),
    WESTERN_LONGBOW(5, Technology.Group.WESTERN, UnitType.INFANTRY, "Western Longbow", 0, 0, 1, 0, 1, 1),
    WESTERN_MEDIEVAL_INFANTRY(1, Technology.Group.WESTERN, UnitType.INFANTRY, "Western Medieval Infantry", 0, 0, 0, 0, 1, 1),
    WESTERN_MEDIEVAL_KNIGHTS(1, Technology.Group.WESTERN, UnitType.CAVALRY, "Western Medieval Knights", 0, 0, 1, 0, 1, 1),
    WESTERN_MEN_AT_ARMS(5, Technology.Group.WESTERN, UnitType.INFANTRY, "Western Men-At-Arms", 0, 0, 0, 1, 1, 1),
    WESTERNIZED_ADAL(26, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Westernized Adal Tactics", 4, 4, 2, 2, 3, 4),
    WESTERNIZED_AZTEC(26, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Westernized Aztec Tactics", 3, 4, 2, 3, 3, 4),
    WESTERNIZED_BANTU(26, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Westernized Bantu Tactics", 4, 3, 3, 3, 3, 3),
    WESTERNIZED_ETHIOPIAN(26, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Westernized Ethiopian", 3, 4, 2, 4, 3, 3),
    WESTERNIZED_INCAN(26, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Westernized Incan", 4, 3, 3, 3, 3, 3),
    WESTERNIZED_MAYAN(30, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Westernized Mayan Tactics", 4, 3, 3, 3, 3, 4),
    WESTERNIZED_NIGER_KONGOLESE(26, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Westernized Niger-Kongolese", 3, 3, 3, 3, 4, 3),
    WESTERNIZED_SOUTH_AMERICAN(26, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Westernized South American Tactics", 3, 4, 2, 3, 3, 4),
    WESTERNIZED_ZAPOTEC(26, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Westernized Zapotec Tactics", 4, 3, 4, 3, 3, 2),
    ZAPOROGHIAN_COSSACK(14, Technology.Group.EASTERN, UnitType.CAVALRY, "Zaporoghian Cossack", 0, 1, 4, 2, 2, 3),
    ZAPOTEC_GUNPOWDER_WARFARE(14, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Zapotec Gunpowder Warfare", 2, 1, 1, 2, 3, 3),
    ZAPOTEC_PLAINS_WARFARE(10, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Zapotec Plains Warfare", 0, 0, 1, 2, 3, 2),
    ZAPOTEC_TRIBAL_WARFARE(5, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Zapotec Tribal Warfare", 0, 0, 1, 1, 2, 2);

    /**
     * Military technology level the unit becomes available (in the range of
     * 0-32)
     */
    public final int tech_level;
    /**
     * {@link TechnologyGroup} of the Unit. All artillery will be considered
     * WESTERN tech by convention (it is available to all tech groups).
     */
    public final Technology.Group tech_group;
    /** Type of the Unit, one of INFANTRY, CAVALRY, ARTILLERY */
    public final UnitType type;
    /** Name of the unit, such as "Western Longbow" */
    public final String name;
    /** Offensive fire pips (0-6) */
    public final int off_fire;
    /** Defensive fire pips (0-6) */
    public final int def_fire;
    /** Offensive shock pips (0-6) */
    public final int off_shock;
    /** Defensive shock pips (0-6) */
    public final int def_shock;
    /** Offensive morale pips (0-6) */
    public final int off_morale;
    /** Defensive morale pips (0-6) */
    public final int def_morale;

    private Unit(int tech_level, Technology.Group techgroup, UnitType type, String name, int off_fire, int def_fire,
	    int off_shock, int def_shock, int off_morale, int def_morale) {
	this.tech_level = tech_level;
	this.tech_group = techgroup;
	this.type = type;
	this.name = name;
	this.off_fire = off_fire;
	this.def_fire = def_fire;
	this.off_shock = off_shock;
	this.def_shock = def_shock;
	this.off_morale = off_morale;
	this.def_morale = def_morale;
    }

    /**
     * @return Pips of the unit for the given phase (FIRE/SHOCK/MORALE) and
     *         combat type (OFFENSE/DEFENSE)
     */
    public int getPips(Phase phase, CombatType combatType) {
	if (combatType == CombatType.OFFENSE && phase == Phase.FIRE) { return off_fire; }
	if (combatType == CombatType.DEFENSE && phase == Phase.FIRE) { return def_fire; }
	if (combatType == CombatType.OFFENSE && phase == Phase.SHOCK) { return off_shock; }
	if (combatType == CombatType.DEFENSE && phase == Phase.SHOCK) { return def_shock; }
	if (combatType == CombatType.OFFENSE && phase == Phase.FIRE) { return off_fire; }
	if (combatType == CombatType.DEFENSE && phase == Phase.FIRE) { return def_fire; }
	if (combatType == CombatType.OFFENSE && phase == Phase.MORALE) { return off_morale; }
	if (combatType == CombatType.DEFENSE && phase == Phase.MORALE) { return def_morale; }
	return 0;
    }

    /**
     * Convenience method to find a Unit of the given name. If no unit of the
     * exact same name exists, <code>null</code> is returned
     */
    public static Unit fromString(String name) {
	return Stream.of(Unit.values()).filter(u -> u.name.equalsIgnoreCase(name)).findAny().orElse(null);
    }

    /**
     * @return The {@link #name} (don't confuse with {@link #name()} from
     *         {@link Enum}!
     */
    public String getName() {
	return name;
    }

    public String getTooltip() {
	StringBuilder sb = new StringBuilder();
	// sb.append("(Pips: (off. fire,def. fire,off. shock, def. shock, off.
	// morale, def. morale): \n");
	sb.append("[" + off_fire + ",");
	sb.append(def_fire + ",");
	sb.append(off_shock + ",");
	sb.append(def_fire + ",");
	sb.append(off_morale + ",");
	sb.append(def_morale + "]");
	sb.append(" = " + Integer.toString(off_fire + def_fire + off_shock + def_shock + off_morale + def_morale));
	return sb.toString();
    }
}