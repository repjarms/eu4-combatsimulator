package eu4.comsim.core.datatypes;

/**
 * Type of combat, either offense or defense. Used for retrieving pips of units
 * or during Battle logic
 */
public enum CombatType {
    OFFENSE,
    DEFENSE
}
