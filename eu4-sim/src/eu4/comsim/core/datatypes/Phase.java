package eu4.comsim.core.datatypes;

/**
 * Combat phases in EU4. <i>Morale</i> is modeled as an individual "phase" ( for
 * purposes of applying morale damage, amongst others)
 */
public enum Phase {

    FIRE("Fire", 0),
    SHOCK("Shock", 1),
    MORALE("Morale", 2);

    /** Name of the phase */
    public final String name;
    /** Unique integer identifier, corresponds to index in Phase.values() */
    public final int id;

    private Phase(String name, int id) {
	this.name = name;
	this.id = id;
    }
}
