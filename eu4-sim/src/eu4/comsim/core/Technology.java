package eu4.comsim.core;

import java.util.stream.Stream;

import eu4.comsim.core.datatypes.Phase;
import eu4.comsim.core.datatypes.UnitType;

/**
 * Represents a nation's technology in EU4.
 */
public class Technology {

    /**
     * @return The technology group
     */
    public Group getGroup() {
	return group;
    }

    /**
     * Technology group
     */
    public enum Group {
	OTTOMAN("Anatolian", 0.5, "/tech/Ottoman.png"),
	CHINESE("Chinese", 0.5, "/tech/Chinese.png"),
	EASTERN("Eastern", 0.6, "/tech/Eastern.png"),
	/**
	 * "Fantasy" tech group, appears only under very special circumstances
	 */
	HIGHAMERICAN("High-American", 0.5, "/tech/High_American.png"),
	INDIAN("Indian", 0.5, "/tech/Indian.png"),
	MESOAMERICAN("Meso-American", 0.5, "/tech/Mesoamerican.png"),
	MUSLIM("Muslim", 0.8, "/tech/Muslim.png"),
	NOMAD_GROUP("Nomadic", 1.0, "/tech/Nomadic.png"),
	NORTH_AMERICAN("North-American", 0.5, "/tech/North_American.png"),
	SOUTH_AMERICAN("South-American", 0.5, "/tech/South_American.png"),
	SUB_SAHARAN("Sub-Saharan", 0.5, "/tech/Sub-Saharan.png"),
	WESTERN("Western", 0.5, "/tech/Western.png");

	public final String name;
	/**
	 * Determines potential insufficient support penalty
	 *
	 * @see <a href=
	 *      "http://www.eu4wiki.com/Land_warfare#Insufficient_support">http:
	 *      //www.eu4wiki.com/Land_warfare#Insufficient_support</a>
	 */
	public final double maxCavalryRatio;
	public final String imagePath;

	private Group(String name, double insufficientSupportRatio, String imagePath) {
	    this.name = name;
	    this.maxCavalryRatio = insufficientSupportRatio;
	    this.imagePath = imagePath;
	}

	/**
	 * Convenience method to find a Technology.Group of the given name. If
	 * no Group of the exact same name exists, <code>null</code> is returned
	 */
	public static Group fromString(String name) {
	    return Stream.of(Group.values()).filter(g -> g.name.equalsIgnoreCase(name)).findAny().orElse(null);
	}
    }

    // TODO legacy, could be nice to have all these typesafe in MIL_TECH_X enum
    // also
    public static final double[] artilleryFire = { 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, 1.00, 1.00, 1.00,
	    1.00, 1.00, 1.40, 1.40, 1.40, 2.40, 2.40, 2.40, 2.40, 2.40, 2.40, 4.40, 4.40, 4.40, 6.40, 6.40, 6.40, 6.40,
	    6.40, 6.40, 6.40, 8.40 };
    public static final double[] artilleryShock = { 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05,
	    0.05, 0.05, 0.15, 0.15, 0.15, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.35, 0.35, 0.35, 0.45, 0.45, 0.45, 0.45,
	    0.45, 0.45, 0.45, 0.55 };
    public static final double[] cavalryFire = { 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.50,
	    0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
	    1.00, 1.00, 1.00 };
    public static final double[] cavalryShock = { 0.80, 0.80, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 2.00, 2.00, 2.00,
	    2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00,
	    4.00, 4.00, 5.00, 5.00 };
    public static final int[] combatWidth = { 15, 15, 20, 20, 20, 22, 24, 24, 25, 25, 25, 27, 27, 27, 29, 29, 30, 30,
	    32, 32, 34, 34, 36, 36, 38, 38, 40, 40, 40, 40, 40, 40, 40 };
    public static final double[] flankingRange = { 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.25,
	    1.25, 1.25, 1.25, 1.25, 1.25, 1.25, 1.50, 1.75, 1.75, 1.75, 1.75, 1.75, 2.25, 2.25, 2.25, 2.25, 2.25, 2.50,
	    2.50, 2.75, 2.75, 2.75 };
    public static final double[] infantryFire = { 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.35, 0.60, 0.60, 0.60,
	    0.60, 0.60, 0.60, 1.10, 1.10, 1.10, 1.10, 1.10, 1.10, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 2.10, 2.10,
	    2.10, 2.10, 3.10, 3.10 };
    public static final double[] infantryShock = { 0.20, 0.30, 0.50, 0.50, 0.50, 0.65, 0.95, 0.95, 0.95, 0.95, 0.95,
	    1.15, 1.15, 1.15, 1.15, 1.15, 1.15, 1.15, 1.15, 1.15, 1.15, 1.65, 1.65, 1.65, 1.65, 1.65, 1.65, 1.65, 2.15,
	    2.15, 2.15, 2.15, 2.15 };
    public static final double[] morale = { 2.00, 2.00, 2.00, 2.50, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00,
	    3.00, 3.00, 3.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 5.00, 5.00, 5.00, 5.00,
	    6.00, 6.00, 6.00 };
    public static final double[] tactics = { 0.50, 0.50, 0.50, 0.50, 0.75, 0.75, 1.00, 1.25, 1.50, 1.50, 1.50, 1.50,
	    1.75, 1.75, 1.75, 2.00, 2.00, 2.00, 2.00, 2.50, 2.50, 2.50, 2.50, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00,
	    3.50, 3.50, 3.50 }; // TODO Wiki difference
    private double discipline;
    private double infCombatAbility, cavCombatAbility, artCombatAbility;
    private int level;

    private double moraleModifier;
    private Group group;

    public Technology(int level) {
	this(level, Group.WESTERN, 1.0, 1.0, 1.0, 1.0, 1.0);
    }

    /**
     * Self explanatory.
     */
    public Technology(int level, Group group, double discipline, double moraleModifier, double infCA, double cavCA,
	    double artCA) {
	this.level = level;
	this.group = group;

	this.discipline = discipline;
	this.moraleModifier = moraleModifier;
	this.infCombatAbility = infCA;
	this.cavCombatAbility = cavCA;
	this.artCombatAbility = artCA;
    }

    public double casualtyMultiplier(UnitType unit, Phase phase) {
	return getCombatModifier(phase, unit) * getCombatAbility(unit) * getDiscipline();
    }

    private double getCombatAbility(UnitType type) {
	switch (type) {
	case INFANTRY:
	    return infCombatAbility;
	case CAVALRY:
	    return cavCombatAbility;
	case ARTILLERY:
	    return artCombatAbility;
	default:
	    return 1.0;
	}
    }

    public double getCombatModifier(Phase phase, UnitType type) {
	if (phase == Phase.FIRE) {
	    if (type == UnitType.INFANTRY) { return infantryFire[level]; }
	    if (type == UnitType.CAVALRY) { return cavalryFire[level]; }
	    if (type == UnitType.ARTILLERY) { return artilleryFire[level]; }
	} else if (phase == Phase.SHOCK) {
	    if (type == UnitType.INFANTRY) { return infantryShock[level]; }
	    if (type == UnitType.CAVALRY) { return cavalryShock[level]; }
	    if (type == UnitType.ARTILLERY) { return artilleryShock[level]; }
	} else {
	    return morale[level];
	}

	throw new IllegalArgumentException("Unknown combat modifier");
    }

    public int getCombatWidth() {
	return combatWidth[level];
    }

    public double getDiscipline() {
	return discipline;
    }

    public double getFlankingRange() {
	return flankingRange[level];
    }

    public int getLevel() {
	return level;
    }

    public double getMorale() {
	return morale[level] * moraleModifier;
    }

    public double getMoraleModifier() {
	return moraleModifier;
    }

    public double getTactics() {
	return tactics[level] * discipline;
    }
}
