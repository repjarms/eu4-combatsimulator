### EU4 Combat Simulator ###

* Combat simulator for Europa Universalis IV by Paradox Interactive
* Version 0.1.1 (alpha)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How to contribute ###

Don't be afraid to hop in and contribute - everyone can help:

* Contribute code via git
* Ideas for new features
* Report bugs
* Point out wrongly modeled combat logic
* Code review